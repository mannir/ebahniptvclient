import shutil
from sys import platform as platform

file1 = '/Users/mannir/Library/Application Support/Kodi/userdata/autoexec.py'
file2 = '/Users/mannir/git/ebahniptvclient/ebahniptv/assets/userdata/autoexec.py'
video1 = '/Users/mannir/Library/Application Support/Kodi/userdata/test.mp4'
video2 = '/Users/mannir/git/ebahniptvclient/ebahniptv/assets/userdata/test.mp4'
db1 = '/Users/mannir/Library/Application Support/Kodi/userdata/ebahniptv/ebahniptv.db'
db2 = '/Users/mannir/git/ebahniptvclient/ebahniptv/assets/userdata/ebahniptv.db'

if platform=='darwin':
    print 'darwin'
    skin1 = '/Users/mannir/Library/Application Support/Kodi/addons/skin.ebahniptv'
    skin2 = '/Users/mannir/git/ebahniptvclient/ebahniptv/assets/addons/skin.ebahniptv'
    script1 = '/Users/mannir/Library/Application Support/Kodi/userdata/ebahniptv'
    script2 = '/Users/mannir/git/ebahniptvclient/ebahniptv/assets/userdata/ebahniptv'
    image1 = '/Users/mannir/Library/Application Support/Kodi/userdata/images/'
    image2 = '/Users/mannir/git/ebahniptvclient/ebahniptv/assets/userdata/images/'
    media1 = 'C:/Users/Mannir/AppData/Roaming/Kodi/addons/skin.ebahniptv/media'
    media2 = '/Users/mannir/git/ebahniptvclient/media'
elif platform == 'linux':
    print 'ubuntu'
    
'''
else:
    skindir1 = 'C:/Users/Mannir/AppData/Roaming/Kodi/addons/skin.ebahniptv'
    skindir2 = 'D:/ebahniptv/src/ebahniptv/assets/addons/skin.ebahniptv'
    media1 = 'C:/Users/Mannir/AppData/Roaming/Kodi/addons/skin.ebahniptv/media'
    media2 = 'D:/ebahniptv/src/media'
'''

def copyDirectory(src, dest):
    try:
        shutil.copytree(src, dest)
    except shutil.Error as e:
        print('Directory not copied. Error: %s' % e)
    except OSError as e:
        print('Directory not copied. Error: %s' % e)

##try: shutil.rmtree(media2)
##except: print ''
try: shutil.rmtree(skin2)
except: print ''
try: shutil.rmtree(script2)
except: print ''
try: shutil.rmtree(image2)
except: print ''
#try: shutil.rmtree(media2)
#except: print 'Cannot Delete!'
copyDirectory(skin1, skin2)
copyDirectory(script1, script2)
copyDirectory(image1, image2)
##copyDirectory(media1, media2)
shutil.copyfile(file1, file2)
shutil.copyfile(db1, db2)
shutil.copyfile(video1, video2)
print 'Finished!'