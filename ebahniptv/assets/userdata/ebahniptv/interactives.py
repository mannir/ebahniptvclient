import urllib
import urllib2
import socket
import os, sys, re
import datetime
import xbmcplugin
import xbmcgui
import xbmcaddon
import xbmc
from default import IPTV
from default import db
from string import whitespace

socket.setdefaulttimeout(5) 

iptv = IPTV()
b = {}


ACTION_PREVIOUS_MENU = 10 # Esc
ACTION_NAV_BACK = 92 # Backspace

ALIGN_CENTER = 6

img = 'special://userdata/images/icons/'

newmsg1 = img+'newmsg2.png';
newmsg2 = img+'newmsg1.png';
readmsg1 = img+'readmsg2.png';
readmsg2 = img+'readmsg1.png';
gmsg1 = img+'gmsg2.png';
gmsg2 = img+'gmsg1.png';
msgtemp1 = img+'msgtemp2.png';
msgtemp2 = img+'msgtemp1.png';


msg_btn = img+'message.png';
send_btn = img+'sendmsg.png';
background_img = 'special://userdata/images/menus/guest.jpg'
button_nf_img = 'button-nofocus.png'
button_fo_img = 'button-focus.png'
ip = xbmc.getInfoLabel(xbmc.getIPAddress())
#xbmc.executebuiltin('Notification(eBahn IPTV,'+str(p1)+',1000,/logo.gif)')
font = 'cirrus_20'
font2 = 'cirrus_40_Bold'
roomno = 0;
guestname ='';
myip = xbmc.getInfoLabel(xbmc.getIPAddress())

class MyAddon(xbmcgui.WindowDialog):
    def __init__(self):
        self.dashboard()
        #self.messagetype()
        
        #self.set_controls()
        self.set_navigation()

    def set_controls(self):
        self.privet_btn = xbmcgui.ControlButton(10, 10, 110, 40, u'Back', focusTexture=button_fo_img,
                                                        noFocusTexture=button_nf_img, alignment=ALIGN_CENTER)
        self.addControl(self.privet_btn)
        #self.exit_btn = xbmcgui.ControlButton(650, 500, 110, 40, u'Previous', focusTexture=button_fo_img,
        #                                                noFocusTexture=button_nf_img, alignment=ALIGN_CENTER)
        #self.addControl(self.exit_btn)

    def set_navigation(self):
        #self.privet_btn.controlRight(self.exit_btn)
        #self.privet_btn.controlLeft(self.exit_btn)
        #self.exit_btn.controlRight(self.privet_btn)
        #self.exit_btn.controlLeft(self.privet_btn)
        self.sendmsg.controlLeft(self.viewmsg)
        self.sendmsg.controlRight(self.viewmsg)
        self.viewmsg.controlLeft(self.sendmsg)
        self.viewmsg.controlRight(self.sendmsg)
        #self.gmsg.controlLeft(self.viewmsg)
        #self.gmsg.controlRight(self.msgtemp)
        #self.msgtemp.controlLeft(self.viewmsg)
        #self.msgtemp.controlRight(self.sendmsg)
               
        self.setFocus(self.sendmsg)

    def onAction(self, action):
        if action == ACTION_NAV_BACK or action == ACTION_PREVIOUS_MENU:
            self.close()
            #xbmc.executebuiltin('ActivateWindow(4444)')

    def onControl(self, control):
        if control == self.viewmsg: self.listmessage()
        if control == self.sendmsg: self.messagetype()
        #else: self.close()

    
    def makeWindow(self):

        headline = xbmcgui.ControlLabel(10,20,300,50,"Recordings on mannir","font14")
        self.addControl(headline)

        self.list = xbmcgui.ControlList(500,300, self.getWidth()-100, self.getHeight()-80)
        self.list.setPageControlVisible(True)
        self.addControl(self.list)
        self.setFocus(self.list)
        
    def dashboard(self):
        self.addControl(xbmcgui.ControlImage(350, 100, 800, 500, 'contentpanel.png'))
        #self.addControl(xbmcgui.ControlImage(0, 0, self.getWidth(), self.getHeight(), 'special://userdata/images/bg.jpg'))
        #self.addControl(xbmcgui.ControlImage(0, 0, self.getWidth(), self.getHeight(), 'special://skin/backgrounds/bg/vod.jpg'))
        self.addControl(xbmcgui.ControlLabel(550, 120, 1000, 50, 'Interactives Services',font2))
        
        self.sendmsg = xbmcgui.ControlButton(500, 200, 150, 100,'', focusTexture=newmsg2, noFocusTexture=newmsg1)
        self.addControl(self.sendmsg)
        self.addControl(xbmcgui.ControlLabel(500, 300, 500, 50, 'Send Message', 'cirrus_20_Bold'))
    
        self.viewmsg = xbmcgui.ControlButton(800, 200, 150, 100,'', focusTexture=readmsg2, noFocusTexture=readmsg1)
        self.addControl(self.viewmsg)
        self.addControl(xbmcgui.ControlLabel(800, 300, 500, 50, 'View Message', 'cirrus_20_Bold'))
        
        #self.gmsg = xbmcgui.ControlButton(700, 200, 200, 150, u'Group Message', focusTexture=gmsg2, noFocusTexture=gmsg1,alignment=6)
        #self.addControl(self.gmsg)
        '''
        self.msgtemp = xbmcgui.ControlButton(800, 200, 200, 150, u'Message Templates', focusTexture=msgtemp2, noFocusTexture=msgtemp1,alignment=6)
        self.addControl(self.msgtemp)
        self.addControl(xbmcgui.ControlLabel(440, 400, 500, 50, 'Guest Folio', 'cirrus_20_Bold'))
        ''' 
        self.setFocus(self.sendmsg)      
        
    def messages(self):
        kb = xbmc.Keyboard('default', 'heading')
        kb.setDefault('') # optional
        kb.setHeading('Enter Room No (Press OK to cancel)') # optional
        #kb.setHiddenInput(True) # optional
        kb.doModal()
        if (kb.getText()!=''):
            ip = '192.168.0.' + kb.getText()
            kb = xbmc.Keyboard('default', 'heading')
            kb.setDefault('I need Laundry now!') # optional
            kb.setHeading('Enter Message to '+str(ip)+' press OK to cancel or Send') # optional
        #kb.setHiddenInput(True) # optional
            kb.doModal()
            if (kb.isConfirmed()):
                message = kb.getText()
                                
                msg = message.replace(' ', '%20')
                url = "%7B%22jsonrpc%22%3A%222.0%22%2C%22method%22%3A%22GUI.ShowNotification%22%2C%22params%22%3A%7B%22title%22%3A%22Message%20from%20"+myip+"%22%2C%22message%22%3A%22"+msg+"%22%7D%2C%22id%22%3A1%7D%22)"
                status = urllib2.urlopen('http://'+ip+':8080/jsonrpc?request='+url).read()
                xbmc.log("eBahnIPTV: "+status)
                #client.GUI.ActivateWindow({"window":"weather"})
                iptv.sendMessage(myip, ip, message)
                xbmc.executebuiltin('Notification(eBahnIPTV,Message sent to '+ip+',10000,/logo.gif)')
            #else:
                #self.close()
                #self.dashboard()
       # else:
            #self.close()
            #self.dashboard()
                
    def listmessage(self):
        try:
            msgs = [''];
            cursor = db.execute("SELECT * FROM messages")
            for row in cursor:
                id = str(row[0])
                fr = str(row[1])
                to = str(row[2])
                msg = str(row[3])
                date = str(row[4])
                msgcontent = id+"    From: "+fr+"  Received on:  "+date+"  Message: "+msg
                msgs.append(msgcontent)
            dialog = xbmcgui.Dialog()
            ret = dialog.select('Messages', msgs)
            if (ret):
                my_message = str(msgs[ret]).split("Message: ",1)[1]
                xbmcgui.Dialog().ok('Message:',str(my_message))
                iptv.sendMessage(myip, ip, my_message)

        except:
            xbmc.executebuiltin('Notification(eBahn IPTV,unable to fetch data from eBahn Server,1000,/logo.gif)')
        
    def messagetype(self):
        try:
            msgs = [''];
            cursor = db.execute("SELECT * FROM messagetype WHERE type='guest'")
            for row in cursor:
                id = str(row[0])
                name = str(row[1])
                msg = str(row[2])
                msgs.append(msg)
            dialog = xbmcgui.Dialog()
            ret = dialog.select('Messages', msgs)
            if (ret):
                #my_message = str(msgs[ret]).split("Message: ",1)[1]
                my_message = str(msgs[ret])
                xbmcgui.Dialog().ok('Message:',str(my_message))
                iptv.sendMessage(myip, ip, my_message)

        except:
            xbmc.executebuiltin('Notification(eBahn IPTV,unable to fetch data from eBahn Server,1000,/logo.gif)')
        
    def msgtemp(self):
        self.addControl(xbmcgui.ControlImage(50, 100, 1200, 500, 'ContentPanel.png'))
        self.addControl(xbmcgui.ControlLabel(200, 120, 1000, 50, 'eBahn IPTV Interactives Services: Message Templates',font2))
        
    def groupmsg(self):
        kb = xbmc.Keyboard('default', 'heading')
        kb.setDefault('') # optional
        kb.setHeading('Enter Room No separate by space! (to cancel leave blank and Press OK)') # optional
        #kb.setHiddenInput(True) # optional
        kb.doModal()
        if (kb.getText()!=''):
            ip = '192.168.0.' + kb.getText()
            kb = xbmc.Keyboard('default', 'heading')
            kb.setDefault('we have to goto lunch now!') # optional
            kb.setHeading('Enter Message to groups (to cancel leav blacnk and press OK)') # optional
        #kb.setHiddenInput(True) # optional
            kb.doModal()
            if (kb.isConfirmed()):
                xbmc.executebuiltin('Notification(eBahnIPTV,Message sent to groups thank you for using eBahnIPTV,10000,/logo.gif)')
                message = kb.getText()
                                
                msg = message.replace(' ', '%20')
                url = "%7B%22jsonrpc%22%3A%222.0%22%2C%22method%22%3A%22GUI.ShowNotification%22%2C%22params%22%3A%7B%22title%22%3A%22Message%20from%20"+myip+"%22%2C%22message%22%3A%22"+msg+"%22%7D%2C%22id%22%3A1%7D%22)"
                status = urllib2.urlopen('http://'+ip+':8080/jsonrpc?request='+url).read()
                xbmc.log("eBahnIPTV: "+status)
                #client.GUI.ActivateWindow({"window":"weather"})
                iptv.sendMessage(myip, ip, message)
                xbmc.executebuiltin('Notification(eBahnIPTV,Message sent to groups thank you for using eBahnIPTV,10000,/logo.gif)')
            #else:
                #self.close()
                #self.dashboard()
       # else:
            #self.close()
            #self.dashboard()
            
if __name__ == '__main__':
    addon = MyAddon()
    addon.doModal()
    del addon
