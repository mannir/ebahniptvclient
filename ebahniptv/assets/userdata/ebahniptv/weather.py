import json, urllib, urllib2
import os, sys, re, socket, datetime, pprint
import xbmc, xbmcgui, xbmcplugin, xbmcaddon
from default import IPTV

iptv = IPTV()

weather = iptv.getWeather('55862650')
bg = ''
icon = ''
#print weather['temp']


location = weather['title']
degree = weather['temp']
text = weather['text']
imgloc = 'special://userdata/images/weather/'

def getBg(degree):
    degree = round(degree)
    if degree >= 40: return imgloc+'weather1.jpg'
    elif degree >= 35: return imgloc+'weather2.jpg'
    elif degree >= 30: return imgloc+'weather3.jpg'
    elif degree >= 25: return imgloc+'weather4.jpg'
    else: return imgloc+'weather5.jpg'

def getIcon(degree):
    degree = round(degree)
    if degree >= 40: return imgloc+'icon1.png'
    elif degree >= 35: return imgloc+'icon2.png'
    elif degree >= 30: return imgloc+'icon3.png'
    elif degree >= 25: return imgloc+'icon4.png'
    else: return imgloc+'icon5.png'
        
bg = getBg(float(degree))
icon = getIcon(float(degree))
#get actioncodes from https://github.com/xbmc/xbmc/blob/master/xbmc/guilib/Key.h
ACTION_PREVIOUS_MENU = 10
ACTION_NAV_BACK = 92 # Backspace

 
class MyClass(xbmcgui.Window):
  def __init__(self):
    self.addControl(xbmcgui.ControlImage(0, 0, self.getWidth(), self.getHeight(), bg))
    self.addControl(xbmcgui.ControlImage(50, 150, 200, 150, icon))
    self.addControl(xbmcgui.ControlLabel(50, 50, 1000, 50, location,'cirrus_40_Bold'))
    self.addControl(xbmcgui.ControlLabel(50, 100, 1000, 50, text,'cirrus_40_Bold'))
    self.addControl(xbmcgui.ControlLabel(500, 100, 1000, 50, degree,'cirrus_40_Bold'))
    self.addControl(xbmcgui.ControlLabel(550, 100, 1000, 50, 'o'))
    self.addControl(xbmcgui.ControlLabel(565, 100, 1000, 50, 'C','cirrus_40_Bold'))
    #self.addControl(xbmcgui.ControlImage(50, 100, 1200, 500, 'special://userdata/images/weather.png'))

  def onAction(self, action):
    if action == ACTION_NAV_BACK or action == ACTION_PREVIOUS_MENU:
      self.close()
 
  def onControl(self, control):
    if control == self.list:
      item = self.list.getSelectedItem()
      self.message('You selected : ' + item.getLabel())  
 
  def message(self, message):
    dialog = xbmcgui.Dialog()
    dialog.ok(" My message title", message)
 
mydisplay = MyClass()
mydisplay.doModal()
del mydisplay
