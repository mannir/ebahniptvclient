
import socket
import os, sys, re, stat
import datetime
import xbmcplugin
import xbmcgui
import xbmcaddon
import xbmc
import mysql.connector
from ebahniptv import sql
from ebahniptv import db
from ebahniptv import dbhost

import time
from Dialog import Dialog



########################### TEST ######################################

#################################################################


#if(os.name=='nt'): dir = '127.0.0.1';
#else: dir = '192.168.0.2'

#serveraddr = "http://192.168.0.2:1313/eBahnIPTVServer/"
serveraddr = "http://127.0.0.1:1313/eBahnIPTVServer/"

d = {}


imgloc = 'special://userdata/scripts/images/';
img = 'special://userdata/images/';

'''
try:
    sql.execute("SELECT * FROM `_vod`")
    i = 0
    for row in sql.fetchall():
        vod[i][0] = row[0]
        vod[i][1] = row[1]
        vod[i][2] = row[2]
        vod[i][3] = row[3]
        vod[i][4] = row[4]
        vod[i][5] = row[5]
        vod[i][6] = row[6]
        vod[i][7] = row[7]
        i += 1

except Exception, e: print e
'''
ACTION_PREVIOUS_MENU = 10 # Esc
ACTION_NAV_BACK = 92 # Backspace
ALIGN_CENTER = 6

background_img = 'special://userdata/images/guest0.jpg'
button_nf_img = 'button-nofocus.png'
button_fo_img = 'button-focus.png'
font = 'cirrus_20'
font2 = 'cirrus_40_Bold'
ip = xbmc.getInfoLabel(xbmc.getIPAddress())
font = 'cirrus_16'
roomno = 0;
guestname ='';

listoptions = ['']; 

selectedOption =[]

class MyAddon(xbmcgui.WindowDialog):
    def __init__(self):
        self.dialog = xbmcgui.Dialog()
        self.dashboard() 
        
        
        #self.checkmark =xbmcgui.ControlCheckMark (150, 300, 200, 50, 'Status', font='font14')
        #self.addControl(self.checkmark) 
        
       

    def onAction(self, action):
        if action == ACTION_NAV_BACK or action == ACTION_PREVIOUS_MENU:
            
            self.close()
        
        
    def dashboard(self):
        self.addControl(xbmcgui.ControlImage(0, 0, self.getWidth(), self.getHeight(), 'special://userdata/images/menubg.png'))    
        self.addControl(xbmcgui.ControlImage(220, 10, 900, 650, 'ContentPanel.png'))
        
        xright=700
        xleft=300
        par=sys.argv[1]
        y=60
        name= par
        qty= 1
        
        try:
            sql.execute("SELECT * FROM `_menu` WHERE name='"+str(name)+"'")          
            for row in sql.fetchall():
                id = row[0]
                name = row[1]
                desc = row[2]
                img1 = row[3]
                img2 = row[4]
                restaurant = row[6]
                price = row[5]
                orderoptions=row[7]        
                #desc='Opens the given window.The parameter[CR] window can either be the windows id, or in the[CR] case of a standard window, the windows name.'
                #price='19.90'   
            order_options = str(orderoptions)
            options = order_options.split(",")
            for option in options:
                sql.execute("SELECT name from `_orderoptions` where option_id='"+str(option)+"'")              
                for row in sql.fetchall():
                        oname=row[0]
                        listoptions.append(oname)

        except: xbmcgui.Dialog().ok("eBahnIPTV", "Cannot connect to Database!")        
        
        d['image'] = xbmcgui.ControlImage(xleft, y, 300, 300, img1)
        d['name']  = xbmcgui.ControlLabel(xleft, y+300, 300, 50,name, alignment=2)
        d['desc']  = xbmcgui.ControlLabel(xleft, y+350, 300, 50,desc, alignment=2)
        d['price']  = xbmcgui.ControlButton(xleft, y+500, 300, 40, 'RM'+str(price), focusTexture=img+'button-focus.png', noFocusTexture=img+'button-focus.png', alignment=2)
        d['qlab']  = xbmcgui.ControlLabel(xright, y+300, 300, 50,'Click To Select Quantity')
        d['quantity']  = xbmcgui.ControlButton(xright, y+350, 200, 40, str(qty), focusTexture=img+'btn.png', noFocusTexture=img+'btn2.png', alignment=2)
        d['send']  = xbmcgui.ControlButton(xright+220, y+500, 150, 40,'Send Order', focusTexture=img+'btn.png', noFocusTexture=img+'btn2.png', alignment=2)
        d['add']  = xbmcgui.ControlButton(xright, y+500, 200, 40,'Add & Continue', focusTexture=img+'btn.png', noFocusTexture=img+'btn2.png', alignment=2)
        d['optionlab']  = xbmcgui.ControlLabel(xright, y, 300, 50,'Please Select Options')      
        d['options']  = xbmcgui.ControlButton(xright, y+50, 200, 40, 'Add Options', focusTexture=img+'btn.png', noFocusTexture=img+'btn2.png', alignment=2)
        d['clear']  = xbmcgui.ControlButton(xright+220, y+50, 180, 40,'Clear Options', focusTexture=img+'btn.png', noFocusTexture=img+'btn2.png', alignment=2)
        self.clist =xbmcgui.ControlList (xright, y+90, 200, 250, font='font14')
        self.addControl(self.clist)  
        
 
        self.addControl(d['image'] )
        self.addControl(d['name'] )
        self.addControl(d['desc'] )
        self.addControl(d['price'] )
        self.addControl(d['qlab'] )
        self.addControl(d['quantity'] )
        self.addControl(d['send'] )
        self.addControl(d['add'] )
        self.addControl(d['optionlab'])
        self.addControl(d['clear'] )
        self.addControl(d['options'] )
        
        d["send"].controlLeft(d["add"])
        d["add"].controlLeft(d["send"])
        
        d["send"].controlUp(d["quantity"])
        d["add"].controlUp(d["quantity"])
        
        d["quantity"].controlDown(d["send"])

        d["send"].controlRight(d["add"])
        d["add"].controlRight(d["send"])
        
        self.setFocus(d['quantity'])
        
        x=850
        x2=1000
        y=200    
        #order_options = str(orderoptions)
        #options = order_options.split(",")
        #for i in options:
            #print i        
            #self.i = xbmcgui.ControlRadioButton(x, y, 120, 40,'', font='font14') 
            #self.i = xbmcgui.ControlButton(x, y, 120, 40,str(i), focusTexture=img+'btn.png', noFocusTexture=img+'btn2.png', alignment=2)           
           # self.addControl(self.i)            
            #self.i.setLabel(str(i))            
            #self.i = xbmcgui.ControlButton(x2, y, 120, 40,'Cancel', focusTexture=img+'btn.png', noFocusTexture=img+'btn2.png', alignment=2)           
            #self.addControl(self.i)           
            #self.i.addItem(str(i))
            
           #y+= 40
    def remove_duplicates(self,values):
        output = []
        seen = set()
        for value in values:
            # If value has not been encountered yet,
            # ... add it to both list and set.
            if value not in seen:
                output.append(value)
                seen.add(value)
        return output
         
    def onControl(self, control):
     
        if control == d['options']: 
            dialog = xbmcgui.Dialog()
            ret = dialog.select('Options', listoptions)           
            if (ret):            
                selectedOption.append(str(listoptions[ret]))  
                              
                selectedOptions = self.remove_duplicates(selectedOption)
                self.clist.addItems(selectedOptions)                                      
        
        elif control == d['clear']: 
            self.clist.reset()
            del selectedOption[:]
            
        elif control == d['send']: 
            n = d['name'].getLabel()
            q = d['quantity'].getLabel()
            p= d['price'].getLabel()
            dialog = xbmcgui.Dialog()
            qty = dialog.yesno('Please Confirm Your Order', n+' :'+ q+':'+p+'[CR]Order 2 : Quantity: Price[CR]Order 3 : Quantity: Price[CR]Order 4 : Quantity: Price','','','Back','Confirm')
            #xbmc.executebuiltin('XBMC.RunScript(special://userdata/scripts/confirmorder.py)')
        elif control == d['add']: 
          
            #xbmcgui.Dialog().ok("My Orders", filepath+'\\myoders.txt')   
            #os.chmod(filepath+'\\myoders.txt', stat.S_IRWXO)        
            order_name = d['name'].getLabel()
            order_qty = d['quantity'].getLabel()
            order_price= d['price'].getLabel()
            filepath = os.getcwd()
            os.path.isfile('myoders.txt') 
            fo = open(filepath.replace("\\", "/")+"//myoders.txt", "ab+")
            fo.write("Name: "+ order_name+" QTY: " + order_qty+ " Price: "+ order_price+"\n");
            # Close opend file
            fo.close()  
                  
            # Open a file
            fo = open(filepath.replace("\\", "/")+"//myoders.txt", "r+")
            str22 = fo.read();
            #print str
            # Close opend file
            fo.close()
                      
            # Delete file test2.txt
            #os.remove("C:/Users/Lenovo/AppData/Roaming/XBMC/userdata/scripts/myorders.txt")        
            #xbmcgui.Dialog().ok("My Orders", str(str22))   
         
            self.close()
            #xbmc.executebuiltin('XBMC.RunScript(special://userdata/scripts/order.py'))
            
        elif control == d['quantity']: 
            dialog = xbmcgui.Dialog()
            qty = dialog.numeric(0, 'Enter Quantity')
            d['quantity'].setLabel(str(qty))
        #elif control == d['quantity']: xbmc.executebuiltin('XBMC.ActivateWindow(109)')
       

if __name__ == '__main__':
    addon = MyAddon()
    addon.doModal()
    del addon

