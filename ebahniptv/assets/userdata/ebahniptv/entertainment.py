import urllib
import urllib2
import socket
import os, sys, re
import datetime
import xbmcplugin
import xbmcgui
import xbmcaddon
import xbmc

d = {}

imgloc = 'special://userdata/images/icons/';

ch = [[],[[],
      ['ch1','150',imgloc+'browser1.png',imgloc+'browser2.png','udp://@238.69.70.1:1234'],
      ['ch2','350',imgloc+'calc1.png',imgloc+'calc2.png','udp://@238.69.70.2:1234'],
      #['ch3','550',imgloc+'calendar1.png',imgloc+'calendar2.png'],
      ['ch3','750',imgloc+'game11.png',imgloc+'game22.png'],
      ['ch4','950',imgloc+'alarm1.png',imgloc+'alarm2.png']],
      
      [[],
      ['ch6','150',imgloc+'fb1.png', imgloc+'fb2.png'],
      ['ch7','350',imgloc+'twitter1.png', imgloc+'twitter2.png'],
      ['ch8','550',imgloc+'youtube1.png', imgloc+'youtube2.png'],
      ['ch9','750',imgloc+'skype1.png', imgloc+'skype2.png'],
      #['ch10','950',imgloc+'maps1.jpg', imgloc+'maps2.jpg']
      ],
      
      [[],
      ['ch11','150',imgloc+'google1.png',imgloc+'google2.png'],
      ['ch12','350',imgloc+'gmail1.png',imgloc+'gmail2.png'],
      ['ch13','550',imgloc+'game11.png',imgloc+'game22.png'],
      ['ch14','750',imgloc+'settings1.png',imgloc+'settings2.png'],
      #['ch15','950',imgloc+'settings1.png',imgloc+'settings2.png']
      ],

    ['3.61503670628', '5.64553650642', '1.39648965337']]

ACTION_PREVIOUS_MENU = 10 # Esc
ACTION_NAV_BACK = 92 # Backspace
ALIGN_CENTER = 6

background_img = 'special://userdata/images/guest0.jpg'
button_nf_img = 'button-nofocus.png'
button_fo_img = 'button-focus.png'
font = 'cirrus_20'
font2 = 'cirrus_40_Bold'
ip = xbmc.getInfoLabel(xbmc.getIPAddress())
font = 'cirrus_16'
roomno = 0;
guestname ='';

class MyAddon(xbmcgui.WindowDialog):
    def __init__(self):
        self.dialog = xbmcgui.Dialog()
        self.dashboard()   

    def onAction(self, action):
        if action == ACTION_NAV_BACK or action == ACTION_PREVIOUS_MENU:
            self.close()
            #xbmc.executebuiltin('ActivateWindow(4444)')
            
    def onFocus(self, control):
        if control == d['ch1']:
            xbmcgui.Dialog().ok("eBahnIPTV", "ch1")
        
    def dashboard(self):
        self.addControl(xbmcgui.ControlImage(300, 100, 800, 500, 'contentpanel.png'))
        #self.addControl(xbmcgui.ControlImage(0, 0, self.getWidth(), self.getHeight(), imgloc+'bg.jpg'))
        #self.addControl(xbmcgui.ControlLabel(400, 100, 1000, 50, 'eBahn IPTV TV Channels',font2))
        x=450
        y=150
        w = 100
        h = 80
        d['ch1'] = xbmcgui.ControlButton(x, y, w, h, '', focusTexture=ch[1][1][2], noFocusTexture=ch[1][1][3])
        d['ch2'] = xbmcgui.ControlButton(x+200, y, w, h, '', focusTexture=ch[1][2][2], noFocusTexture=ch[1][2][3])
        d['ch3'] = xbmcgui.ControlButton(x+400, y, w, h, '', focusTexture=ch[1][3][2], noFocusTexture=ch[1][3][3])
        #d['ch4'] = xbmcgui.ControlButton(x+600, y, w, h, '', focusTexture=ch[1][4][2], noFocusTexture=ch[1][4][3])
        #d['ch5'] = xbmcgui.ControlButton(x*5, y, 150, 150, '', focusTexture=ch[1][5][2], noFocusTexture=ch[1][5][3])
        
        d['ch6'] = xbmcgui.ControlButton(x, y*2, w, h, '', focusTexture=ch[2][1][2], noFocusTexture=ch[2][1][3])
        d['ch7'] = xbmcgui.ControlButton(x+200, y*2, w, h, '', focusTexture=ch[2][2][2], noFocusTexture=ch[2][2][3])
        d['ch8'] = xbmcgui.ControlButton(x+400, y*2, w, h, '', focusTexture=ch[2][3][2], noFocusTexture=ch[2][3][3])
        #d['ch9'] = xbmcgui.ControlButton(x+600, y*2, w, h, '', focusTexture=ch[2][4][2], noFocusTexture=ch[2][4][3])
        #d['ch10'] = xbmcgui.ControlButton(x*5, y*2, 150, 150, '', focusTexture=ch[2][5][2], noFocusTexture=ch[2][5][3])
        
        d['ch11'] = xbmcgui.ControlButton(x, y*3, w, h, '', focusTexture=ch[3][1][2], noFocusTexture=ch[3][1][3])
        d['ch12'] = xbmcgui.ControlButton(x+200, y*3, w, h, '', focusTexture=ch[3][2][2], noFocusTexture=ch[3][2][3])
        d['ch13'] = xbmcgui.ControlButton(x+400, y*3, w, h, '', focusTexture=ch[3][3][2], noFocusTexture=ch[3][3][3])
        #d['ch14'] = xbmcgui.ControlButton(x+600, y*3, w, h, '', focusTexture=ch[3][4][2], noFocusTexture=ch[3][4][3])
        #d['ch15'] = xbmcgui.ControlButton(x*5, y*3, 150, 150, '', focusTexture=ch[3][5][2], noFocusTexture=ch[3][5][3])       

        self.addControl(d['ch1'])
        self.addControl(d['ch2'])
        self.addControl(d['ch3'])
        #self.addControl(d['ch4'])
        #self.addControl(d['ch5'])
        self.addControl(d['ch6'])
        self.addControl(d['ch7'])
        self.addControl(d['ch8'])
        #self.addControl(d['ch9'])
        #self.addControl(d['ch10'])
        self.addControl(d['ch11'])
        self.addControl(d['ch12'])
        self.addControl(d['ch13'])
        #self.addControl(d['ch14'])
        #self.addControl(d['ch15'])
        
        d["ch1"].controlRight(d["ch2"])
        d["ch2"].controlRight(d["ch3"])
        d["ch3"].controlRight(d["ch6"])
        #d["ch4"].controlRight(d["ch6"])
        #d["ch5"].controlRight(d["ch6"])
        d["ch6"].controlRight(d["ch7"])
        d["ch7"].controlRight(d["ch8"])
        d["ch8"].controlRight(d["ch11"])
        #d["ch9"].controlRight(d["ch11"])
        #d["ch10"].controlRight(d["ch11"])
        d["ch11"].controlRight(d["ch12"])
        d["ch12"].controlRight(d["ch13"])
        d["ch13"].controlRight(d["ch1"])
        #d["ch14"].controlRight(d["ch1"])
        #d["ch15"].controlRight(d["ch1"])
                
        d["ch1"].controlLeft(d["ch13"])
        d["ch2"].controlLeft(d["ch1"])
        d["ch3"].controlLeft(d["ch6"])
        #d["ch4"].controlLeft(d["ch3"])
        #d["ch5"].controlLeft(d["ch4"])
        d["ch6"].controlLeft(d["ch3"])
        d["ch7"].controlLeft(d["ch6"])
        d["ch8"].controlLeft(d["ch7"])
        #d["ch9"].controlLeft(d["ch8"])
        #d["ch10"].controlLeft(d["ch9"])
        d["ch11"].controlLeft(d["ch8"])
        d["ch12"].controlLeft(d["ch11"])
        d["ch13"].controlLeft(d["ch12"])
        #d["ch14"].controlLeft(d["ch13"])
        #d["ch15"].controlLeft(d["ch14"])
        
        d["ch1"].controlDown(d["ch6"])
        d["ch2"].controlDown(d["ch7"])
        d["ch3"].controlDown(d["ch8"])
        #d["ch4"].controlDown(d["ch9"])
        #d["ch5"].controlDown(d["ch10"])
        d["ch6"].controlDown(d["ch11"])
        d["ch7"].controlDown(d["ch12"])
        d["ch8"].controlDown(d["ch13"])
        #d["ch9"].controlDown(d["ch14"])
        #d["ch10"].controlDown(d["ch15"])
        d["ch11"].controlDown(d["ch1"])
        d["ch12"].controlDown(d["ch2"])
        d["ch13"].controlDown(d["ch3"])
        #d["ch14"].controlDown(d["ch4"])
        #d["ch15"].controlDown(d["ch5"])
        
        d["ch1"].controlUp(d["ch11"])
        d["ch2"].controlUp(d["ch12"])
        d["ch3"].controlUp(d["ch13"])
        #d["ch4"].controlUp(d["ch14"])
        #d["ch5"].controlUp(d["ch15"])
        d["ch6"].controlUp(d["ch1"])
        d["ch7"].controlUp(d["ch2"])
        d["ch8"].controlUp(d["ch3"])
        #d["ch9"].controlUp(d["ch4"])
        #d["ch10"].controlUp(d["ch5"])
        d["ch11"].controlUp(d["ch6"])
        d["ch12"].controlUp(d["ch7"])
        d["ch13"].controlUp(d["ch8"])
        #d["ch14"].controlUp(d["ch9"])
        #d["ch15"].controlUp(d["ch10"])
        
        self.setFocus(d['ch1'])
        
    def onControl(self, control):
        if control == d['ch1']: xbmc.executebuiltin('XBMC.StartAndroidActivity("com.inostudio.alarm_clock.RootActivity")')
        elif control == d['ch2']: xbmc.executebuiltin('XBMC.StartAndroidActivity("com.android.calculator2")')
        elif control == d['ch3']: xbmc.executebuiltin('XBMC.StartAndroidActivity("com.android.calendar")')
        #elif control == d['ch4']: xbmc.executebuiltin('XBMC.StartAndroidActivity("com.android.deskclock")')
        #elif control == d['ch5']: xbmc.executebuiltin('XBMC.RunScript(special://userdata/scripts/alarm.py)')
        elif control == d['ch6']: xbmcgui.Dialog().ok("eBahnIPTV", "app not install, sorry for inconvenience")
        elif control == d['ch7']: xbmcgui.Dialog().ok("eBahnIPTV", "app not install, sorry for inconvenience")
        elif control == d['ch8']: xbmcgui.Dialog().ok("eBahnIPTV", "app not install, sorry for inconvenience")
        #elif control == d['ch9']: xbmc.executebuiltin('XBMC.StartAndroidActivity("com.skype.raider")')
        #elif control == d['ch10']: xbmc.executebuiltin('XBMC.StartAndroidActivity("com.google.android.apps.maps")')
        elif control == d['ch11']: xbmcgui.Dialog().ok("eBahnIPTV", "app not install, sorry for inconvenience")
        elif control == d['ch12']: xbmcgui.Dialog().ok("eBahnIPTV", "app not install, sorry for inconvenience")
        elif control == d['ch13']: xbmcgui.Dialog().ok("eBahnIPTV", "app not install, sorry for inconvenience")
        #elif control == d['ch14']: xbmcgui.Dialog().ok("eBahnIPTV", "app not install, sorry for inconvenience")
        #elif control == d['ch15']: xbmcgui.Dialog().ok("eBahnIPTV", "function not available, sorry for inconvenience!")

if __name__ == '__main__':
    addon = MyAddon()
    addon.doModal()
    del addon
