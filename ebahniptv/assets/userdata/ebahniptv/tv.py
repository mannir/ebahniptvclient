import urllib
import urllib2
import socket
import os, sys, re
import datetime
import xbmcplugin
import xbmcgui
import xbmcaddon
import xbmc
import pprint
import sqlite3
from default import db

d = {}
img = 'special://userdata/scripts/channels/'

n = 50; ch = [[['' for k in xrange(n)] for j in xrange(n)] for i in xrange(n)]

################################### TESTING ###############################################
#pprint.pprint(distance)
try:
    channels = db.execute("SELECT * FROM channels WHERE id>=1 AND id<=5")
    i=1
    for row in channels:
        #print str(i)+'='+ row[1]
        #ch[1][i][0] = row[0]; # chname
        ch[1][i][1] = row[1]; # fc
        ch[1][i][2] = row[2]; # fc
        ch[1][i][3] = row[4]; # nf
        ch[1][i][4] = row[5]; # udp
        ch[1][i][5] = row[6]; # udp
        ch[1][i][6] = row[10]; # status
        #print ch[1][i][6]
        #
        #print str(i)+'\t'+ ch[1][i][0] + '\t' + ch[1][i][1] + '\t' + ch[1][i][2] + '\t' + ch[1][i][3] + '\t' + ch[1][i][4]+ '\t'+ch[1][i][5]
        i+=1
except Exception, e: print e

try:
    channels = db.execute("SELECT * FROM channels WHERE id>=6 AND id<=10")
    i=1
    for row in channels:
        #print str(i)+'='+ row[1]
        #ch[1][i][0] = row[0]; # chname
        ch[2][i][1] = row[1]; # fc
        ch[2][i][2] = row[2]; # fc
        ch[2][i][3] = row[4]; # nf
        ch[2][i][4] = row[5]; # udp
        ch[2][i][5] = row[6]; # udp
        ch[2][i][6] = row[10];
        
        #
        #print str(i)+'\t'+ ch[2][i][0] + '\t' + ch[2][i][1] + '\t' + ch[2][i][2] + '\t' + ch[2][i][3] + '\t' + ch[2][i][4]+ '\t'+ch[2][i][5]+ '\t'+ch[2][i][6]
        i+=1
except Exception, e: print e

try:
    channels = db.execute("SELECT * FROM channels WHERE id>=11 AND id<=15")
    i=1
    for row in channels:
        #print str(i)+'='+ row[1]
        #ch[1][i][0] = row[0]; # chname
        ch[3][i][1] = row[1]; # fc
        ch[3][i][2] = row[2]; # fc
        ch[3][i][3] = row[4]; # nf
        ch[3][i][4] = row[5]; # udp
        ch[3][i][5] = row[6]; # udp
        ch[3][i][6] = row[10];

        #
        #print str(i)+'\t'+ ch[1][i][0] + '\t' + ch[1][i][1] + '\t' + ch[1][i][2] + '\t' + ch[1][i][3] + '\t' + ch[1][i][4]+ '\t'+ch[1][i][5]
        i+=1
except Exception, e: print e

#for i in range(1,6):
#    print str(i)+'\t'+ ch[3][i][0] + '\t' + ch[3][i][1] + '\t' + ch[3][i][2] + '\t' + ch[3][i][3] + '\t' + ch[3][i][4]+ '\t'+ch[3][i][5]

#print ch[1][2][4]


#:
print ch[1][5][5]

#for i in range(0,len(ch)):
#    print ch[i]
#exit()
#print ch[2][i][5]
##########################################################################################
ACTION_PREVIOUS_MENU = 10 # Esc
ACTION_NAV_BACK = 92 # Backspace
ALIGN_CENTER = 6

background_img = 'special://userdata/images/guest0.jpg'
button_nf_img = 'button-nofocus.png'
button_fo_img = 'button-focus.png'
font = 'cirrus_20'
font2 = 'cirrus_40_Bold'
ip = xbmc.getInfoLabel(xbmc.getIPAddress())
font = 'cirrus_16'
roomno = 0;
guestname ='';

class MyAddon(xbmcgui.WindowDialog):
    def __init__(self):
        self.dialog = xbmcgui.Dialog()
        self.dashboard()   

    def onAction(self, action):
        if action == ACTION_NAV_BACK or action == ACTION_PREVIOUS_MENU:
            self.close()
            xbmc.Player().stop();
            #xbmc.executebuiltin('ActivateWindow(4444)')
            
    def onFocus(self, control):
        if control == d['ch1']:
            xbmcgui.Dialog().ok("eBahnIPTV", "ch1")
        
    def dashboard(self):
        self.addControl(xbmcgui.ControlImage(170, 100, 950, 500, 'special://userdata/images/contentpanel.png'))
        #self.addControl(xbmcgui.ControlImage(0, 0, self.getWidth(), self.getHeight(), 'special://userdata/images/bg.jpg'))
        #self.addControl(xbmcgui.ControlLabel(400, 50, 1000, 50, 'eBahn IPTV TV Channels',font2))
        x=200
        y=150
        
        d['ch1'] = xbmcgui.ControlButton(x, y, 150, 100, '', focusTexture=ch[1][1][3], noFocusTexture=ch[1][1][4],alignment=6,textColor='ff00ff7f')
        d['ch2'] = xbmcgui.ControlButton(x+180, y, 150, 100, '', focusTexture=ch[1][2][3], noFocusTexture=ch[1][2][4],alignment=6,textColor='ff00ff7f')
        d['ch3'] = xbmcgui.ControlButton(x+360, y, 150, 100, '', focusTexture=ch[1][3][3], noFocusTexture=ch[1][3][4],alignment=6,textColor='ff00ff7f')
        d['ch4'] = xbmcgui.ControlButton(x+540, y, 150, 100, '', focusTexture=ch[1][4][3], noFocusTexture=ch[1][4][4],alignment=6,textColor='ff00ff7f')
        d['ch5'] = xbmcgui.ControlButton(x+720, y, 150, 100, '', focusTexture=ch[1][5][3], noFocusTexture=ch[1][5][4],alignment=6,textColor='ff00ff7f')
        
        d['ch6'] = xbmcgui.ControlButton(x, y*2, 150, 100, '', focusTexture=ch[2][1][3], noFocusTexture=ch[2][1][4],alignment=6,textColor='ff00ff7f')
        d['ch7'] = xbmcgui.ControlButton(x+180, y*2, 150, 100, '', focusTexture=ch[2][2][3], noFocusTexture=ch[2][2][4],alignment=6,textColor='ff00ff7f')
        d['ch8'] = xbmcgui.ControlButton(x+360, y*2, 150, 100, '', focusTexture=ch[2][3][3], noFocusTexture=ch[2][3][4],alignment=6,textColor='ff00ff7f')
        d['ch9'] = xbmcgui.ControlButton(x+540, y*2, 150, 100, '', focusTexture=ch[2][4][3], noFocusTexture=ch[2][4][4],alignment=6,textColor='ff00ff7f')
        d['ch10'] = xbmcgui.ControlButton(x+720, y*2, 150, 100, '', focusTexture=ch[2][5][3], noFocusTexture=ch[2][5][4],alignment=6,textColor='ff00ff7f')
        
        d['ch11'] = xbmcgui.ControlButton(x, y*3, 150, 100, '', focusTexture=ch[3][1][3], noFocusTexture=ch[3][1][4],alignment=6,textColor='ff00ff7f')
        d['ch12'] = xbmcgui.ControlButton(x+180, y*3, 150, 100, '', focusTexture=ch[3][2][3], noFocusTexture=ch[3][2][4],alignment=6,textColor='ff00ff7f')
        d['ch13'] = xbmcgui.ControlButton(x+360, y*3, 150, 100, '', focusTexture=ch[3][3][3], noFocusTexture=ch[3][3][4],alignment=6,textColor='ff00ff7f')
        d['ch14'] = xbmcgui.ControlButton(x+540, y*3, 150, 100, '', focusTexture=ch[3][4][3], noFocusTexture=ch[3][4][4],alignment=6,textColor='ff00ff7f')
        d['ch15'] = xbmcgui.ControlButton(x+720, y*3, 150, 100, '', focusTexture=ch[3][5][3], noFocusTexture=ch[3][5][4],alignment=6,textColor='ff00ff7f')       

        self.addControl(d['ch1'])
        self.addControl(d['ch2'])
        self.addControl(d['ch3'])
        self.addControl(d['ch4'])
        self.addControl(d['ch5'])
        self.addControl(d['ch6'])
        self.addControl(d['ch7'])
        self.addControl(d['ch8'])
        self.addControl(d['ch9'])
        self.addControl(d['ch10'])
        self.addControl(d['ch11'])
        self.addControl(d['ch12'])
        self.addControl(d['ch13'])
        self.addControl(d['ch14'])
        self.addControl(d['ch15'])
        
        d["ch1"].controlRight(d["ch2"])
        d["ch2"].controlRight(d["ch3"])
        d["ch3"].controlRight(d["ch4"])
        d["ch4"].controlRight(d["ch5"])
        d["ch5"].controlRight(d["ch6"])
        d["ch6"].controlRight(d["ch7"])
        d["ch7"].controlRight(d["ch8"])
        d["ch8"].controlRight(d["ch9"])
        d["ch9"].controlRight(d["ch10"])
        d["ch10"].controlRight(d["ch11"])
        d["ch11"].controlRight(d["ch12"])
        d["ch12"].controlRight(d["ch13"])
        d["ch13"].controlRight(d["ch14"])
        d["ch14"].controlRight(d["ch15"])
        d["ch15"].controlRight(d["ch1"])
                
        d["ch1"].controlLeft(d["ch15"])
        d["ch2"].controlLeft(d["ch1"])
        d["ch3"].controlLeft(d["ch2"])
        d["ch4"].controlLeft(d["ch3"])
        d["ch5"].controlLeft(d["ch4"])
        d["ch6"].controlLeft(d["ch5"])
        d["ch7"].controlLeft(d["ch6"])
        d["ch8"].controlLeft(d["ch7"])
        d["ch9"].controlLeft(d["ch8"])
        d["ch10"].controlLeft(d["ch9"])
        d["ch11"].controlLeft(d["ch10"])
        d["ch12"].controlLeft(d["ch11"])
        d["ch13"].controlLeft(d["ch12"])
        d["ch14"].controlLeft(d["ch13"])
        d["ch15"].controlLeft(d["ch14"])
        
        d["ch1"].controlDown(d["ch6"])
        d["ch2"].controlDown(d["ch7"])
        d["ch3"].controlDown(d["ch8"])
        d["ch4"].controlDown(d["ch9"])
        d["ch5"].controlDown(d["ch10"])
        d["ch6"].controlDown(d["ch11"])
        d["ch7"].controlDown(d["ch12"])
        d["ch8"].controlDown(d["ch13"])
        d["ch9"].controlDown(d["ch14"])
        d["ch10"].controlDown(d["ch15"])
        d["ch11"].controlDown(d["ch1"])
        d["ch12"].controlDown(d["ch2"])
        d["ch13"].controlDown(d["ch3"])
        d["ch14"].controlDown(d["ch4"])
        d["ch15"].controlDown(d["ch5"])
        
        d["ch1"].controlUp(d["ch11"])
        d["ch2"].controlUp(d["ch12"])
        d["ch3"].controlUp(d["ch13"])
        d["ch4"].controlUp(d["ch14"])
        d["ch5"].controlUp(d["ch15"])
        d["ch6"].controlUp(d["ch1"])
        d["ch7"].controlUp(d["ch2"])
        d["ch8"].controlUp(d["ch3"])
        d["ch9"].controlUp(d["ch4"])
        d["ch10"].controlUp(d["ch5"])
        d["ch11"].controlUp(d["ch6"])
        d["ch12"].controlUp(d["ch7"])
        d["ch13"].controlUp(d["ch8"])
        d["ch14"].controlUp(d["ch9"])
        d["ch15"].controlUp(d["ch10"])
        
        self.setFocus(d['ch1'])
        
    def onControl(self, control):
        if control == d['ch1']:
            if(str(ch[1][1][6])=='1'):
                self.close()
                #xbmc.executebuiltin('XBMC.PlayMedia('+ch[1][1][5]+')')
                xbmc.executebuiltin('XBMC.RunScript(special://userdata/scripts/playtv.py, '+str(ch[1][1][5])+')')
                self.addControl(xbmcgui.ControlImage(0, 0, 100, 100, str(ch[1][1][3])))
            else: xbmcgui.Dialog().ok("eBahnIPTV", "This Channel is not Available, sorry for inconvenience: ")
            
        elif control == d['ch2']:
            if(str(ch[1][2][6])=='1'):
                self.close()
                #xbmc.executebuiltin('XBMC.PlayMedia('+ch[1][2][5]+')')
                xbmc.executebuiltin('XBMC.RunScript(special://userdata/scripts/playtv.py, '+str(ch[1][2][5])+')')
            else: xbmcgui.Dialog().ok("eBahnIPTV", "This Channel is not Available, sorry for inconvenience:")
            
        elif control == d['ch3']:
            if(str(ch[1][3][6])=='1'):
                self.close()
                #xbmc.executebuiltin('XBMC.PlayMedia('+ch[1][3][5]+')')
                xbmc.executebuiltin('XBMC.RunScript(special://userdata/scripts/playtv.py, '+str(ch[1][3][5])+')')
            else: xbmcgui.Dialog().ok("eBahnIPTV", "This Channel is not Available, sorry for inconvenience")

        elif control == d['ch4']:
            if(str(ch[1][4][6])=='1'):
                self.close()
            #xbmc.executebuiltin('XBMC.PlayMedia('+ch[1][4][5]+')')
                xbmc.executebuiltin('XBMC.RunScript(special://userdata/scripts/playtv.py, '+str(ch[1][4][5])+')')
            else: xbmcgui.Dialog().ok("eBahnIPTV", "This Channel is not Available, sorry for inconvenience")
        
        elif control == d['ch5']:
            if(str(ch[1][5][6])=='1'):
                self.close()
                videourl = str(ch[1][5][5])
                #xbmcgui.Dialog().ok('', videourl)
                #xbmc.executebuiltin('XBMC.RunScript(special://userdata/scripts/playtv.py, '+str(ch[3][5][5])+')')
                #rtmpurl = 'rtmp://aljazeeraflashlivefs.fplive.net:443/aljazeeraflashlive-live?videoId=883816736001&lineUpId=&pubId=665003303001&playerId=751182905001&affiliateId=/aljazeera_eng_med?videoId=883816736001&lineUpId=&pubId=665003303001&playerId=751182905001&affiliateId= live=true'
                li = xbmcgui.ListItem('AlJazeera Live')
                xbmc.Player(xbmc.PLAYER_CORE_DVDPLAYER).play(videourl, li)
            #xbmc.executebuiltin('XBMC.PlayMedia('+ch[1][5][5]+')')
                #xbmc.executebuiltin('XBMC.RunScript(special://userdata/scripts/playtv.py, '+str(ch[1][5][5])+')')
            else: xbmcgui.Dialog().ok("eBahnIPTV", "This Channel is not Available, sorry for inconvenience")
        
        elif control == d['ch6']:
            if(str(ch[2][1][6])=='1'):
                self.close()
                #xbmc.executebuiltin('XBMC.PlayMedia('+ch[1][1][5]+')')
                xbmc.executebuiltin('XBMC.RunScript(special://userdata/scripts/playtv.py, '+str(ch[2][1][5])+')')
            else: xbmcgui.Dialog().ok("eBahnIPTV", "This Channel is not Available, sorry for inconvenience: ")
            
        elif control == d['ch7']:
            if(str(ch[2][2][6])=='1'):
                self.close()
                xbmc.executebuiltin('XBMC.RunScript(special://userdata/scripts/playtv.py, '+str(ch[2][2][5])+')')
            else: xbmcgui.Dialog().ok("eBahnIPTV", "This Channel is not Available, sorry for inconvenience: ")
                
        elif control == d['ch8']:
            if(str(ch[2][3][6])=='1'):
                self.close()
                xbmc.executebuiltin('XBMC.RunScript(special://userdata/scripts/playtv.py, '+str(ch[2][3][5])+')')
            else: xbmcgui.Dialog().ok("eBahnIPTV", "This Channel is not Available, sorry for inconvenience: ")
                
        elif control == d['ch9']:
            if(str(ch[2][4][6])=='1'):
                self.close()
                xbmc.executebuiltin('XBMC.RunScript(special://userdata/scripts/playtv.py, '+str(ch[2][4][5])+')')
            else: xbmcgui.Dialog().ok("eBahnIPTV", "This Channel is not Available, sorry for inconvenience: ")
                
        elif control == d['ch10']:
            if(str(ch[2][4][6])=='1'):
                self.close()
                xbmc.executebuiltin('XBMC.RunScript(special://userdata/scripts/playtv.py, '+str(ch[2][4][5])+')')
            else: xbmcgui.Dialog().ok("eBahnIPTV", "This Channel is not Available, sorry for inconvenience: ")
                
        elif control == d['ch11']:
            if(str(ch[3][1][6])=='1'):
                self.close()
                xbmc.executebuiltin('XBMC.RunScript(special://userdata/scripts/playtv.py, '+str(ch[3][1][5])+')')
            else: xbmcgui.Dialog().ok("eBahnIPTV", "This Channel is not Available, sorry for inconvenience: ")
                
        elif control == d['ch12']:
            if(str(ch[3][2][6])=='1'):
                self.close()
                xbmc.executebuiltin('XBMC.RunScript(special://userdata/scripts/playtv.py, '+str(ch[3][2][5])+')')
            else: xbmcgui.Dialog().ok("eBahnIPTV", "This Channel is not Available, sorry for inconvenience: ")
                
        elif control == d['ch13']:
            if(str(ch[3][3][6])=='1'):
                self.close()
                xbmc.executebuiltin('XBMC.RunScript(special://userdata/scripts/playtv.py, '+str(ch[3][3][5])+')')
            else: xbmcgui.Dialog().ok("eBahnIPTV", "This Channel is not Available, sorry for inconvenience: ")
                
                
        elif control == d['ch14']:
            if(str(ch[3][4][6])=='1'):
                self.close()
                xbmc.executebuiltin('XBMC.RunScript(special://userdata/scripts/playtv.py, '+str(ch[3][4][5])+')')
            else: xbmcgui.Dialog().ok("eBahnIPTV", "This Channel is not Available, sorry for inconvenience: ")
            #xbmc.executebuiltin('XBMC.PlayMedia('+ch[3][4][5]+')')
            '''
            playlist = xbmc.PlayList( xbmc.PLAYLIST_VIDEO )
            playlist.clear()
            playlist.add(str(ch[3][4][5]))
            xbmc.Player().play( playlist)
            xbmc.Player().disableSubtitles();
            '''
            #xbmc.Player().onQueueNextItem()
        elif control == d['ch15']:
            if(str(ch[3][5][6])=='1'):
                videourl = str(ch[3][5][5])
                self.close()
                
                li = xbmcgui.ListItem('TV Channel 15')
                xbmc.Player(xbmc.PLAYER_CORE_DVDPLAYER).play(videourl, li)
                
                #xbmc.executebuiltin('XBMC.RunScript(special://userdata/scripts/playtv.py, '+str(ch[3][5][5])+')')
            else: xbmcgui.Dialog().ok("eBahnIPTV", "This Channel is not Available, sorry for inconvenience: ")
            #xbmcgui.Dialog().ok("eBahnIPTV", "This Channel is not Available, sorry for inconvenience")

if __name__ == '__main__':
    addon = MyAddon()
    addon.doModal()
    del addon
