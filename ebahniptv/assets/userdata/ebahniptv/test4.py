import os, sys, urllib2
from default import db
from string import whitespace
import json
import random

from default import IPTV
iptv = IPTV()
watched = iptv.getWatched('192.168.0.5')

#if(sys.argv[1]):
#    print sys.argv[1]

serveraddress = 'http://10.0.0.88:1313/ebahniptv/'

def sendMessage(fromaddr, toaddr, msg):
    message = msg.replace(" ", "%20")
    req = urllib2.Request(serveraddress+'json?q=newmessage&from='+fromaddr+'&to='+toaddr+'&message='+message , None, headers={})
    resp = urllib2.urlopen(req).read()
    msg = resp.translate(None, '"').translate(None, whitespace)
    return msg

#print sendMessage('192.168.0.1','192.168.0.2','hello')


def getMessages(ip):
    serveraddress1 = 'http://10.0.0.88:1313/ebahniptv/'
    try:
        n = 50; msg = [[['' for k in xrange(n)] for j in xrange(n)] for i in xrange(n)]
        req = urllib2.Request(serveraddress1+'json?q=messages')
        opener = urllib2.build_opener()
        f = opener.open(req)
        msg = json.loads(f.read())
    except: print ''
    return msg

print getMessages('192.168.0.1')