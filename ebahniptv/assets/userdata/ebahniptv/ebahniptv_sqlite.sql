/*
 Navicat Premium Data Transfer

 Source Server         : ebahniptv
 Source Server Type    : SQLite
 Source Server Version : 3007015
 Source Database       : main

 Target Server Type    : SQLite
 Target Server Version : 3007015
 File Encoding         : utf-8

 Date: 02/24/2015 13:06:19 PM
*/

PRAGMA foreign_keys = false;

-- ----------------------------
--  Table structure for "_channels"
-- ----------------------------
DROP TABLE IF EXISTS "_channels";
CREATE TABLE "_channels" (
"id"  INTEGER(11) NOT NULL,
"chname"  TEXT(10),
"yaxis"  TEXT(10),
"xaxis"  TEXT(11),
"nofocuspicture"  TEXT(50),
"focuspicture"  TEXT(50),
"url"  TEXT(100),
"name"  TEXT(50),
"picture"  TEXT(50),
"watch"  INTEGER(11),
"available"  INTEGER(11),
PRIMARY KEY ("id")
);

-- ----------------------------
--  Records of "_channels"
-- ----------------------------
BEGIN;
INSERT INTO "_channels" VALUES (1, 'ch1', 150, 150, 'special://userdata/images/channels/CH1nf.png', 'special://userdata/images/channels/CH1fc.png', 'udp://@238.69.70.1:1234', 'BBC', 'special://skin/images/channels/CH1.jpg', 34, 1);
INSERT INTO "_channels" VALUES (2, 'ch2', 150, 350, 'special://userdata/images/channels/CH2nf.png', 'special://userdata/images/channels/CH2fc.png', 'udp://@238.69.70.2:1234', 'CHANNEL 2', 'special://skin/images/channels/CH2.png', 2, 1);
INSERT INTO "_channels" VALUES (3, 'ch3', 150, 550, 'special://userdata/images/channels/CH3nf.png', 'special://userdata/images/channels/CH3fc.png', 'udp://@238.69.70.3:1234', 'CHANNEL 3', 'special://skin/images/channels/CH3.jpg', 34, 1);
INSERT INTO "_channels" VALUES (4, 'ch4', 150, 750, 'special://userdata/images/channels/CH4nf.png', 'special://userdata/images/channels/CH4fc.png', 'udp://@238.69.70.4:1234', 'CHANNEL 4', 'special://skin/images/channels/CH4.jpg', 34, 1);
INSERT INTO "_channels" VALUES (5, 'ch5', 150, 950, 'special://userdata/images/channels/CH5nf.png', 'special://userdata/images/channels/CH5fc.png', 'udp://@238.69.70.5:1234', 'CHANNEL 5', 'special://skin/images/channels/CH5.jpg', 23, 1);
INSERT INTO "_channels" VALUES (6, 'ch6', 250, 150, 'special://userdata/images/channels/CH6nf.png', 'special://userdata/images/channels/CH6fc.png', 'udp://@238.69.70.6:1234', 'CHANNEL 6', 'special://skin/images/channels/CH6.jpg', 2, 0);
INSERT INTO "_channels" VALUES (7, 'ch7', 250, 350, 'special://userdata/images/channels/CH7nf.png', 'special://userdata/images/channels/CH7fc.png', 'udp://@238.69.70.7:1234', 'CHANNEL 7', 'special://skin/images/channels/CH7.jpg', 5, 0);
INSERT INTO "_channels" VALUES (8, 'ch8', 250, 550, 'special://userdata/images/channels/CH8nf.png', 'special://userdata/images/channels/CH8fc.png', 'udp://@238.69.70.8:1234', 'CHANNEL 8', 'special://skin/images/channels/CH8.jpg', 2, 0);
INSERT INTO "_channels" VALUES (9, 'ch9', 250, 750, 'special://userdata/images/channels/CH9nf.png', 'special://userdata/images/channels/CH9fc.png', 'udp://@238.69.70.9:1234', 'CHANNEL 9', 'special://skin/images/channels/CH9.jpg', 13, 0);
INSERT INTO "_channels" VALUES (10, 'ch10', 250, 950, 'special://userdata/images/channels/CH10nf.png', 'special://userdata/images/channels/CH10fc.png', 'udp://@238.69.70.10:1234', 'CHANNEL 10', 'special://skin/images/channels/CH10.jpg', 15, 0);
INSERT INTO "_channels" VALUES (11, 'ch11', 350, 150, 'special://userdata/images/channels/CH11nf.png', 'special://userdata/images/channels/CH11fc.png', 'udp://@238.69.70.11:1234', 'CHANNEL 11', 'special://skin/images/channels/CH11.jpg', 0, 0);
INSERT INTO "_channels" VALUES (12, 'ch12', 350, 350, 'special://userdata/images/channels/CH12nf.png', 'special://userdata/images/channels/CH12fc.png', 'udp://@238.69.70.12:1234', 'CHANNEL 12', 'special://skin/images/channels/CH12.jpg', 13, 0);
INSERT INTO "_channels" VALUES (13, 'ch13', 350, 550, 'special://userdata/images/channels/CH13nf.png', 'special://userdata/images/channels/CH13fc.png', 'udp://@238.69.70.13:1234', 'CHANNEL 13', 'special://skin/images/channels/CH13.jpg', 2, 0);
INSERT INTO "_channels" VALUES (14, 'ch14', 350, 750, 'special://userdata/images/channels/CH14nf.png', 'special://userdata/images/channels/CH14fc.png', 'udp://@238.69.70.14:1234', 'CHANNEL 14', 'special://skin/images/channels/CH14.jpg', 1, 0);
INSERT INTO "_channels" VALUES (15, 'ch15', 350, 950, 'special://userdata/images/channels/CH15nf.png', 'special://userdata/images/channels/CH15fc.png', 'udp://@238.69.70.15:1234', 'CHANNEL 15', 'special://skin/images/digitalsignage.jpg', 1, 0);
COMMIT;

-- ----------------------------
--  Table structure for "_clients"
-- ----------------------------
DROP TABLE IF EXISTS "_clients";
CREATE TABLE "_clients" (
"id"  INTEGER(11) NOT NULL,
"roomno"  TEXT(10),
"guestname"  TEXT(50),
"ipaddress"  TEXT(20),
"macaddress"  TEXT(20),
"status"  TEXT(20),
PRIMARY KEY ("id")
);

-- ----------------------------
--  Table structure for "_digitalsignage"
-- ----------------------------
DROP TABLE IF EXISTS "_digitalsignage";
CREATE TABLE "_digitalsignage" (
"id"  INTEGER(11) NOT NULL,
"type"  TEXT(50),
"name"  TEXT,
"days"  TEXT,
"start"  TEXT,
"end"  TEXT,
"plays"  INTEGER(11),
PRIMARY KEY ("id")
);

-- ----------------------------
--  Records of "_digitalsignage"
-- ----------------------------
BEGIN;
INSERT INTO "_digitalsignage" VALUES (1, 'video', 'paint.mp4', '', '10:00:00', '23:59:00', 15);
INSERT INTO "_digitalsignage" VALUES (2, 'video', 'kfc1.mp4', '', '10:00:00', '23:59:00', 2);
INSERT INTO "_digitalsignage" VALUES (3, 'image1', 'special://userdata/images/ds/image1.jpg', 'sun=on,wed=on,thu=on,fri=on,sat=on,', '10:00:00', '23:59:00', 2);
INSERT INTO "_digitalsignage" VALUES (4, 'image2', 'special://userdata/images/ds/ad.jpg', 'sun=on,wed=on,thu=on,fri=on,sat=on,', '10:00:00', '23:59:00', 3);
INSERT INTO "_digitalsignage" VALUES (5, 'image2', 'special://userdata/images/ds/ad2.jpg', 'sun=on,wed=on,thu=on,fri=on,sat=on,', '10:00:00', '23:59:00', 4);
INSERT INTO "_digitalsignage" VALUES (6, 'image2', 'special://userdata/images/ds/ad3.jpg', 'sun=on,wed=on,thu=on,fri=on,sat=on,', '10:00:00', '23:59:00', 0);
INSERT INTO "_digitalsignage" VALUES (7, 'image2', 'special://userdata/images/ds/ad4.jpg', 'sun=on,wed=on,thu=on,fri=on,sat=on,', '10:00:00', '23:59:00', 0);
INSERT INTO "_digitalsignage" VALUES (8, 'text', 'WELCOME TO CENANG PLAZA BEACH HOTEL...', 'sun=on,wed=on,thu=on,fri=on,sat=on,', '10:00:00', '23:59:00', 0);
INSERT INTO "_digitalsignage" VALUES (9, 'video', 'Langkawi1.mp4', '', '08:31:00', '09:31:00', null);
INSERT INTO "_digitalsignage" VALUES (10, 'video', 'kfc2.mp4', '', '08:31:00', '09:31:00', null);
INSERT INTO "_digitalsignage" VALUES (11, 'video', 'kfc3.mp4', '', '08:31:00', '09:31:00', null);
INSERT INTO "_digitalsignage" VALUES (12, 'image3', 'special://userdata/images/ds/image3_1.jpg', null, null, null, null);
INSERT INTO "_digitalsignage" VALUES (13, 'video', 'Langkawi3.mp4', null, null, null, null);
INSERT INTO "_digitalsignage" VALUES (14, 'image1', 'special://userdata/images/ds/image1_1.jpg', null, null, null, null);
INSERT INTO "_digitalsignage" VALUES (15, 'video', 'Langkawi4.mp4', null, null, null, null);
INSERT INTO "_digitalsignage" VALUES (16, 'video1', 'demo.mp4', null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for "_hotelinfo"
-- ----------------------------
DROP TABLE IF EXISTS "_hotelinfo";
CREATE TABLE "_hotelinfo" (
"id"  INTEGER(11) NOT NULL,
"name"  TEXT(50),
"title"  TEXT(50),
"desc"  TEXT,
"image"  TEXT,
PRIMARY KEY ("id")
);

-- ----------------------------
--  Table structure for "_images"
-- ----------------------------
DROP TABLE IF EXISTS "_images";
CREATE TABLE "_images" (
"id"  INTEGER(11) NOT NULL,
"name"  TEXT(20),
"url"  TEXT,
PRIMARY KEY ("id")
);

-- ----------------------------
--  Records of "_images"
-- ----------------------------
BEGIN;
INSERT INTO "_images" VALUES (1, 'welcomebg', 'special://userdata/images/bg1.jpg');
INSERT INTO "_images" VALUES (2, 'mainpagebg', 'special://userdata/images/mainpage.jpg');
INSERT INTO "_images" VALUES (3, 'tvchannelsbg', 'special://userdata/images/tv.jpg');
INSERT INTO "_images" VALUES (4, 'vodbg', 'special://userdata/images/vod.jpg');
INSERT INTO "_images" VALUES (5, 'hotelbg', 'special://userdata/images/hotel.jpg');
INSERT INTO "_images" VALUES (6, 'guestbg', 'special://userdata/images/bg/guestinfo.jpg');
INSERT INTO "_images" VALUES (7, 'entertainmentbg', 'special://userdata/images/entertainment.jpg');
INSERT INTO "_images" VALUES (8, 'interactivesbg', 'special://userdata/images/interactives.jpg');
INSERT INTO "_images" VALUES (9, 'weatherbg', 'special://userdata/images/weather2.jpg');
INSERT INTO "_images" VALUES (10, 'digitalsignagebg', 'special://userdata/images/ds1.png');
INSERT INTO "_images" VALUES (11, 'logoimg', 'special://userdata/images/logo.png');
INSERT INTO "_images" VALUES (12, 'glass1img', 'special://userdata/images/glass1.png');
INSERT INTO "_images" VALUES (13, 'lang1img', 'special://userdata/images/glass1.png');
INSERT INTO "_images" VALUES (14, 'lang2img', 'special://userdata/images/glass1.png');
INSERT INTO "_images" VALUES (15, 'lang3img', 'special://userdata/images/glass1.png');
INSERT INTO "_images" VALUES (16, 'weatherbg2', 'special://userdata/images/weather/1.png');
COMMIT;

-- ----------------------------
--  Table structure for "_licences"
-- ----------------------------
DROP TABLE IF EXISTS "_licences";
CREATE TABLE "_licences" (
"id"  INTEGER(11) NOT NULL,
"type"  TEXT(20),
"code"  TEXT(50),
"expiry"  TEXT,
"status"  TEXT(200),
PRIMARY KEY ("id")
);

-- ----------------------------
--  Records of "_licences"
-- ----------------------------
BEGIN;
INSERT INTO "_licences" VALUES (1, 'client', 11111, '2015-01-15 17:30:34', null);
INSERT INTO "_licences" VALUES (2, 'client', 22222, '2015-01-22 00:43:10', null);
INSERT INTO "_licences" VALUES (3, 'stb', 33333, '2015-02-03 21:30:00', null);
INSERT INTO "_licences" VALUES (4, 'stb', 44444, '2015-02-04 19:38:26', null);
INSERT INTO "_licences" VALUES (5, 'demo', 55555, '2015-06-01 09:00:00', null);
COMMIT;

-- ----------------------------
--  Table structure for "_logs"
-- ----------------------------
DROP TABLE IF EXISTS "_logs";
CREATE TABLE "_logs" (
"id"  INTEGER(11) NOT NULL,
"ip"  TEXT(20),
"mac"  TEXT(20),
"roomno"  TEXT(100),
"date"  TEXT,
PRIMARY KEY ("id")
);

-- ----------------------------
--  Records of "_logs"
-- ----------------------------
BEGIN;
INSERT INTO "_logs" VALUES (1, '192.168.0.166', 'Busy', null, '2014-08-14 09:33:56');
INSERT INTO "_logs" VALUES (2, '192.168.0.166', 'Busy', null, '2014-08-14 09:44:30');
INSERT INTO "_logs" VALUES (3, '192.168.0.4', 'Busy', null, '2014-08-14 17:28:29');
INSERT INTO "_logs" VALUES (4, '192.168.0.7', 'Busy', null, '2014-08-15 18:42:38');
INSERT INTO "_logs" VALUES (5, '192.168.0.7', 'Busy', null, '2014-08-15 18:43:22');
COMMIT;

-- ----------------------------
--  Table structure for "_messages"
-- ----------------------------
DROP TABLE IF EXISTS "_messages";
CREATE TABLE "_messages" (
"id"  INTEGER(11) NOT NULL,
"from"  TEXT(20),
"to"  TEXT(20),
"message"  TEXT,
"date"  TEXT,
PRIMARY KEY ("id")
);

-- ----------------------------
--  Records of "_messages"
-- ----------------------------
BEGIN;
INSERT INTO "_messages" VALUES (1, '192.168.0.4', '192.168.0.6', 'I Need Laundry', '2014-09-12 15:20:38');
INSERT INTO "_messages" VALUES (2, '192.168.0.4', '192.168.0.6', 'I Need Laundry', '2014-09-12 15:20:39');
INSERT INTO "_messages" VALUES (3, '192.168.0.4', '192.168.0.6', 'I Need Laundry', '2014-09-12 15:20:40');
INSERT INTO "_messages" VALUES (4, '192.168.0.8', '192.168.0.3', 'I Need Laundry55', '2014-09-12 15:26:37');
INSERT INTO "_messages" VALUES (5, '192.168.0.8', '192.168.0.3', 'I Need Laundry55', '2014-09-12 15:26:48');
INSERT INTO "_messages" VALUES (6, '192.168.0.8', '192.168.0.3', 'I Need Laundry55', '2014-09-12 15:26:59');
INSERT INTO "_messages" VALUES (7, '192.168.0.8', '192.168.0.85', 'I Need Laundry', '2014-09-12 15:28:39');
INSERT INTO "_messages" VALUES (8, '192.168.0.7', '192.168.0.7', 'testing', '2014-09-12 15:33:09');
INSERT INTO "_messages" VALUES (9, '192.168.0.7', '192.168.0.7', 'testing', '2014-09-12 15:33:13');
INSERT INTO "_messages" VALUES (10, '192.168.0.7', '192.168.0.75', 'testing', '2014-09-12 15:33:24');
INSERT INTO "_messages" VALUES (11, '192.168.0.7', '192.168.0.75', 'testingssftgg ', '2014-09-12 15:33:49');
INSERT INTO "_messages" VALUES (12, '192.168.0.5', '192.168.0.8', ' I Need Laundry right now', '2014-09-26 09:46:31');
INSERT INTO "_messages" VALUES (13, '192.168.0.5', '192.168.0.8', 'jhiI Need Laundry', '2014-09-26 11:04:41');
INSERT INTO "_messages" VALUES (14, '192.168.0.5', '192.168.0.12q8', 'I Need Laundry', '2014-09-26 11:05:46');
INSERT INTO "_messages" VALUES (15, '192.168.0.5', '192.168.0.12oiq8', 'I Need Laundry', '2014-09-26 11:06:51');
COMMIT;

-- ----------------------------
--  Table structure for "_messagetype"
-- ----------------------------
DROP TABLE IF EXISTS "_messagetype";
CREATE TABLE "_messagetype" (
"id"  INTEGER(11) NOT NULL,
"message"  TEXT,
PRIMARY KEY ("id")
);

-- ----------------------------
--  Records of "_messagetype"
-- ----------------------------
BEGIN;
INSERT INTO "_messagetype" VALUES (1, 'I Need Loundry');
INSERT INTO "_messagetype" VALUES (2, 'I Need Cleaning');
INSERT INTO "_messagetype" VALUES (3, 'Need Services');
COMMIT;

-- ----------------------------
--  Table structure for "_modules"
-- ----------------------------
DROP TABLE IF EXISTS "_modules";
CREATE TABLE "_modules" (
"id"  INTEGER(11) NOT NULL,
"name"  TEXT(50),
"title"  TEXT(50),
"icon"  TEXT(255),
"thumb"  TEXT(255),
"onclick"  TEXT(255),
"visible"  TEXT(50),
PRIMARY KEY ("id")
);

-- ----------------------------
--  Records of "_modules"
-- ----------------------------
BEGIN;
INSERT INTO "_modules" VALUES (1, 'welcome', 'Welcome Page', 'special://userdata/images/bg1.jpg', 'special://userdata/images/bg.jpg', 'ActivateWindow(134)', 'true');
INSERT INTO "_modules" VALUES (2, 'main', 'Main Page', 'special://userdata/images/mainpage.jpg', 'special://userdata/images/bg.jpg', 'ActivateWindow(134)', 'true');
INSERT INTO "_modules" VALUES (3, 'tv', 'TV Channels', 'special://userdata/images/tv.jpg', 'special://userdata/images/bg.jpg', 'XBMC.RunScript(special://userdata/scripts/vod.py)', 'true');
INSERT INTO "_modules" VALUES (4, 'vod', 'Video on Demand', 'special://userdata/images/vod.jpg', 'special://userdata/images/bg.jpg', 'XBMC.RunScript(special://userdata/scripts/vod.py)', 'true');
INSERT INTO "_modules" VALUES (5, 'hotel', 'Hotel Info', 'special://userdata/images/hotel.jpg', 'special://userdata/images/bg.jpg', 'XBMC.RunScript(special://userdata/scripts/handler.py, hotelinfo)', 'true');
INSERT INTO "_modules" VALUES (6, 'guest', 'Guest Services', 'special://userdata/images/bg/guestinfo.jpg', 'special://userdata/images/bg.jpg', 'XBMC.RunScript(special://userdata/scripts/guestservices.py)', 'true');
INSERT INTO "_modules" VALUES (7, 'entertainment', 'Entertainment', 'special://userdata/images/entertainment.jpg', 'special://userdata/images/bg.jpg', 'XBMC.RunScript(special://userdata/scripts/entertainment.py)', 'true');
INSERT INTO "_modules" VALUES (8, 'interactives', 'Interactives', 'special://userdata/images/interactives.jpg', 'special://userdata/images/bg.jpg', 'XBMC.RunScript(special://userdata/scripts/interactives.py)', 'true');
INSERT INTO "_modules" VALUES (9, 'weather', 'Weather', 'special://userdata/images/weather2.jpg', 'special://userdata/images/bg.jpg', 'XBMC.RunScript(special://userdata/scripts/weather.py)', 'true');
INSERT INTO "_modules" VALUES (10, 'digitalsig', 'Digital Signage', 'special://userdata/images/ds1.png', 'special://userdata/images/bg.jpg', 'XBMC.RunScript(special://userdata/scripts/digitalsignage.py)', 'true');
INSERT INTO "_modules" VALUES (11, 'help', 'Remote Guide', 'special://userdata/images/bg1.jpg', 'special://userdata/images/bg1.jpg', 'ActivateWindow(134)', 'true');
COMMIT;

-- ----------------------------
--  Table structure for "_modules_orig"
-- ----------------------------
DROP TABLE IF EXISTS "_modules_orig";
CREATE TABLE "_modules_orig" (
"id"  INTEGER(11) NOT NULL,
"name"  TEXT(20),
"status"  TEXT(10),
"title"  TEXT(50),
"image1"  TEXT(100),
"image2"  TEXT(100),
PRIMARY KEY ("id")
);

-- ----------------------------
--  Records of "_modules_orig"
-- ----------------------------
BEGIN;
INSERT INTO "_modules_orig" VALUES (1, 'welcome', 'true', null, null, null);
INSERT INTO "_modules_orig" VALUES (2, 'mainpage', 'false', null, null, null);
INSERT INTO "_modules_orig" VALUES (3, 'tvchannels', 'true', null, null, null);
INSERT INTO "_modules_orig" VALUES (4, 'vod', 'false', null, null, null);
INSERT INTO "_modules_orig" VALUES (5, 'hotelinfo', 'true', null, null, null);
INSERT INTO "_modules_orig" VALUES (6, 'guestinfo', 'false', null, null, null);
INSERT INTO "_modules_orig" VALUES (7, 'entertainment', 'false', null, null, null);
INSERT INTO "_modules_orig" VALUES (8, 'interactives', 'false', null, null, null);
INSERT INTO "_modules_orig" VALUES (9, 'weather', 'false', null, null, null);
INSERT INTO "_modules_orig" VALUES (10, 'digitalsignage', 'true', 'Digital Signage', null, 'special://userdata/images/welcome.jpg');
INSERT INTO "_modules_orig" VALUES (11, 'helpmenu', 'true', null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for "_pictures"
-- ----------------------------
DROP TABLE IF EXISTS "_pictures";
CREATE TABLE "_pictures" (
"id"  INTEGER(11) NOT NULL,
"type"  TEXT(20),
"name"  TEXT,
"url"  TEXT,
PRIMARY KEY ("id")
);

-- ----------------------------
--  Records of "_pictures"
-- ----------------------------
BEGIN;
INSERT INTO "_pictures" VALUES (1, null, 'welcome', 'smb://192.168.0.2/ebahniptv/images/bg.jpg');
COMMIT;

-- ----------------------------
--  Table structure for "_properties"
-- ----------------------------
DROP TABLE IF EXISTS "_properties";
CREATE TABLE "_properties" (
"id"  INTEGER(11) NOT NULL,
"name"  TEXT(50),
"value"  TEXT,
PRIMARY KEY ("id")
);

-- ----------------------------
--  Records of "_properties"
-- ----------------------------
BEGIN;
INSERT INTO "_properties" VALUES (1, 'manager', 'Mr. Ernest');
INSERT INTO "_properties" VALUES (2, 'welcomemsg', '[CR]Welcome to CENANG PLAZA BEACH HOTEL![CR]We would like to thank you for selecting our hotel [CR]for your holidays. It is our pleasure to have you as our guests [CR] and we hope you will have a pleasant to stau with us.[CR]Me personally, my colleagues as well as our staff at the [CR] Hotel are dedicated to make your stay comfortable, relaxing [CR] and a truly pleasant experieance. Your faithfully,');
INSERT INTO "_properties" VALUES (3, 'hotel1name', 'Sea-facing King Room');
INSERT INTO "_properties" VALUES (4, 'hotel1pic', 'special://userdata/images/hotels/p1.jpg');
INSERT INTO "_properties" VALUES (5, 'hotel1desc', null);
INSERT INTO "_properties" VALUES (6, 'hotel2name', 'Sea-facing Deluxe Twin ');
INSERT INTO "_properties" VALUES (8, 'hotel2pic', 'special://userdata/images/hotels/p2.jpg');
INSERT INTO "_properties" VALUES (9, 'hotel2desc', null);
INSERT INTO "_properties" VALUES (10, 'hotel3name', 'Deluxe Twin Room');
INSERT INTO "_properties" VALUES (11, 'hotel3pic', 'special://userdata/images/hotels/p3.jpg');
INSERT INTO "_properties" VALUES (12, 'hotel3desc', null);
INSERT INTO "_properties" VALUES (13, 'hotel4name', 'Single Room');
INSERT INTO "_properties" VALUES (14, 'hotel4pic', 'special://userdata/images/hotels/p4.jpg');
INSERT INTO "_properties" VALUES (15, 'hotel4desc', null);
INSERT INTO "_properties" VALUES (16, 'hotel5name', 'RESTAURANT-THE CLIFF');
INSERT INTO "_properties" VALUES (17, 'hotel5pic', 'special://userdata/images/hotels/p5.jpg');
INSERT INTO "_properties" VALUES (18, 'hotel5desc', null);
INSERT INTO "_properties" VALUES (19, 'hotel6name', 'RESTAURANT');
INSERT INTO "_properties" VALUES (20, 'hotel6pic', 'special://userdata/images/hotels/p6.jpg');
INSERT INTO "_properties" VALUES (21, 'hotel6desc', null);
INSERT INTO "_properties" VALUES (22, 'hotel7name', 'POOL');
INSERT INTO "_properties" VALUES (23, 'hotel7pic', 'smb://192.168.0.2/ebahniptv/VOD/p7.jpg');
INSERT INTO "_properties" VALUES (24, 'hotel7desc', null);
INSERT INTO "_properties" VALUES (25, 'welcometitle', 'WELCOME TO LANGKAWI');
INSERT INTO "_properties" VALUES (26, 'helppic', 'special://userdata/images/hotels/p3.jpg');
INSERT INTO "_properties" VALUES (27, 'helpbg', 'special://userdata/images/remotehelp.png');
INSERT INTO "_properties" VALUES (28, 'helppic2', 'special://userdata/images/bg.jpg');
COMMIT;

-- ----------------------------
--  Table structure for "_purchase"
-- ----------------------------
DROP TABLE IF EXISTS "_purchase";
CREATE TABLE "_purchase" (
"id"  INTEGER(11) NOT NULL,
"ip"  TEXT(20),
"desc"  TEXT,
"amount"  INTEGER(11),
"date"  TEXT,
PRIMARY KEY ("id")
);

-- ----------------------------
--  Records of "_purchase"
-- ----------------------------
BEGIN;
INSERT INTO "_purchase" VALUES (1, '192.168.0.3', 'test.mp4', 15, '2014-09-12 12:03:45');
INSERT INTO "_purchase" VALUES (2, '192.168.0.3', 'test.mp4', 15, '2014-09-12 12:04:29');
INSERT INTO "_purchase" VALUES (3, '192.168.0.3', 'test.mp4', null, '2014-09-12 12:04:40');
INSERT INTO "_purchase" VALUES (4, '192.168.0.5', 'test.mp4', null, '2014-09-12 12:37:35');
INSERT INTO "_purchase" VALUES (5, '192.168.0.5', 'TaylorSwift', null, '2014-09-12 12:38:25');
COMMIT;

-- ----------------------------
--  Table structure for "_reports"
-- ----------------------------
DROP TABLE IF EXISTS "_reports";
CREATE TABLE "_reports" (
"id"  INTEGER(11) NOT NULL,
"marks"  INTEGER(11),
PRIMARY KEY ("id")
);

-- ----------------------------
--  Records of "_reports"
-- ----------------------------
BEGIN;
INSERT INTO "_reports" VALUES (1, 20);
INSERT INTO "_reports" VALUES (2, 53);
INSERT INTO "_reports" VALUES (3, 78);
INSERT INTO "_reports" VALUES (4, 28);
INSERT INTO "_reports" VALUES (5, 46);
COMMIT;

-- ----------------------------
--  Table structure for "_stb"
-- ----------------------------
DROP TABLE IF EXISTS "_stb";
CREATE TABLE "_stb" (
"id"  INTEGER(11) NOT NULL,
"title"  TEXT(20),
"roomno"  TEXT(20),
"mac"  TEXT(20),
"ip"  TEXT(20),
"netmask"  TEXT(20),
"gateway"  TEXT(20),
"dns1"  TEXT(20),
"dns2"  TEXT(20),
"status"  INTEGER(1),
PRIMARY KEY ("id")
);

-- ----------------------------
--  Records of "_stb"
-- ----------------------------
BEGIN;
INSERT INTO "_stb" VALUES (1, 'Server', 1, '', '192.168.0.2', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 0);
INSERT INTO "_stb" VALUES (2, 'Lobby DS 1', 2, '', '192.168.0.80', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (3, 'Room 201', 201, '', '192.168.0.11', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (4, 'Room 202', 202, '', '192.168.0.12', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (5, 'Room 203', 203, '', '192.168.0.13', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (6, 'Room 204', 204, '', '192.168.0.14', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (7, 'Room 205', 205, '', '192.168.0.15', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (8, 'Room 206', 206, '', '192.168.0.16', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (9, 'Room 207', 207, '', '192.168.0.17', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (10, 'Room 208', 208, '', '192.168.0.18', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (11, 'Room 209', 209, '', '192.168.0.19', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (12, 'Room 210', 210, '', '192.168.0.20', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (13, 'Room 211', 211, '', '192.168.0.21', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (14, 'Room 212', 212, '', '192.168.0.22', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (15, 'Room 213', 213, '', '192.168.0.23', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (16, 'Room 214', 214, '', '192.168.0.24', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (17, 'Room 215', 215, '', '192.168.0.25', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (18, 'Room 216', 216, '', '192.168.0.26', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (19, 'Room 217', 217, '', '192.168.0.27', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (20, 'Room 218', 218, '', '192.168.0.28', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (21, 'Room 219', 219, '', '192.168.0.29', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (22, 'Room 220', 220, '', '192.168.0.30', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (23, 'Room 221', 221, null, '192.168.0.31', null, null, null, null, 0);
INSERT INTO "_stb" VALUES (24, 'Room 222', 222, '', '192.168.0.32', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (25, 'Room 223', 223, null, '192.168.0.33', null, null, null, null, 0);
INSERT INTO "_stb" VALUES (26, 'Room 224', 224, '', '192.168.0.34', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (27, 'Room 301', 301, '', '192.168.0.41', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (28, 'Room 302', 302, '', '192.168.0.42', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (29, 'Room 303', 303, '', '192.168.0.43', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (30, 'Room 304', 304, '', '192.168.0.44', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (31, 'Room 305', 305, '', '192.168.0.45', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (32, 'Room 306', 306, '', '192.168.0.46', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (33, 'Room 307', 307, '', '192.168.0.47', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (34, 'Room 308', 308, '', '192.168.0.48', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (35, 'Room 309', 309, '', '192.168.0.49', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (36, 'Room 310', 310, '', '192.168.0.50', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (37, 'Room 311', 311, '', '192.168.0.51', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (38, 'Room 312', 312, '', '192.168.0.52', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (39, 'Room 313', 313, '', '192.168.0.53', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (40, 'Room 314', 314, '', '192.168.0.54', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (41, 'Room 315', 315, '', '192.168.0.55', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (42, 'Room 316', 316, '', '192.168.0.56', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (43, 'Room 317', 317, '', '192.168.0.57', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (44, 'Room 318', 318, '', '192.168.0.58', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (45, 'Room 319', 319, '', '192.168.0.59', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (46, 'Room 320', 320, '', '192.168.0.60', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (48, 'Room 322', 322, '', '192.168.0.62', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (50, 'Room 324', 324, '', '192.168.0.64', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (51, 'Room 401', 401, null, '192.168.0.65', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (52, 'Lobby DS 2', 3, null, '192.168.0.70', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (53, 'Room 402', 402, null, '192.168.0.66', '255.255.0.0', '192.168.0.1', '192.168.0.1', '0.0.0.0', 1);
INSERT INTO "_stb" VALUES (54, 'Server Room', 999, null, '192.168.0.7', null, null, null, null, null);
INSERT INTO "_stb" VALUES (55, 'James', 100, null, '192.168.0.5', null, null, null, null, null);
INSERT INTO "_stb" VALUES (56, 'Mannir Lan', 1003, null, '192.168.0.3', null, null, null, null, null);
INSERT INTO "_stb" VALUES (57, 'Lenovo Laptop', 10004, null, '192.168.0.100', null, null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for "_users"
-- ----------------------------
DROP TABLE IF EXISTS "_users";
CREATE TABLE "_users" (
"id"  INTEGER(11) NOT NULL,
"username"  TEXT(20),
"password"  TEXT(20),
"role"  TEXT(20),
PRIMARY KEY ("id")
);

-- ----------------------------
--  Records of "_users"
-- ----------------------------
BEGIN;
INSERT INTO "_users" VALUES (1, 'admin', 'admin', 'admin');
INSERT INTO "_users" VALUES (2, 'client1', 'client1', 'client');
COMMIT;

-- ----------------------------
--  Table structure for "_values"
-- ----------------------------
DROP TABLE IF EXISTS "_values";
CREATE TABLE "_values" (
"id"  INTEGER(11) NOT NULL,
"name"  TEXT(100),
"values"  TEXT,
PRIMARY KEY ("id")
);

-- ----------------------------
--  Records of "_values"
-- ----------------------------
BEGIN;
INSERT INTO "_values" VALUES (1, 'apklocation0', '/home/ebahn/ebahniptv/INSTALL/ebahniptv.apk');
INSERT INTO "_values" VALUES (2, 'dbhost', '127.0.0.1');
INSERT INTO "_values" VALUES (3, 'dbconfig', 'D:gitebahniptveBahnIPTVServer/ebahniptv.properties');
INSERT INTO "_values" VALUES (4, 'serverip', '192.168.0.2');
INSERT INTO "_values" VALUES (5, 'dbpass', 'ebahn');
INSERT INTO "_values" VALUES (6, 'videoslocation1', 'smb://192.168.0.3/ebahniptv/VOD/');
INSERT INTO "_values" VALUES (7, 'dbuser', 'ebahn');
INSERT INTO "_values" VALUES (8, 'videoslocation0', 'smb://192.168.0.2/ebahniptv/VOD/');
INSERT INTO "_values" VALUES (9, 'apklocation1', 'D:/git/ebahniptv/eBahnIPTVClient/ebahniptv.apk');
INSERT INTO "_values" VALUES (10, 'dbport', 3306);
INSERT INTO "_values" VALUES (11, 'dbname', 'ebahniptv');
INSERT INTO "_values" VALUES (12, 'userdata1', 'D:/git/ebahniptv/eBahnIPTVClient/Code/userdata');
INSERT INTO "_values" VALUES (13, 'imageslocation0', 'smb://192.168.0.2/ebahniptv/images/');
INSERT INTO "_values" VALUES (14, 'dbconfig1', 'D:/gitebahniptveBahnIPTVServer/ebahniptv.properties');
INSERT INTO "_values" VALUES (15, 'digitalsignage0', 'smb://192.168.0.2/ebahniptv/digitalsignage/');
INSERT INTO "_values" VALUES (16, 'imageslocation1', 'smb://192.168.0.3/ebahniptv/images/');
INSERT INTO "_values" VALUES (17, 'digitalsignage1', 'smb://192.168.0.2/ebahniptv/digitalsignage/image0/');
INSERT INTO "_values" VALUES (18, 'userdata0', '/home/ebahn/ebahniptv/INSTALL/userdata');
INSERT INTO "_values" VALUES (19, 'videolength', 10);
COMMIT;

-- ----------------------------
--  Table structure for "_videos"
-- ----------------------------
DROP TABLE IF EXISTS "_videos";
CREATE TABLE "_videos" (
"id"  INTEGER(11) NOT NULL,
"name"  TEXT(50),
"price"  INTEGER(11),
"category"  TEXT(20),
"location"  TEXT(100),
"picture"  TEXT(100),
"watch"  INTEGER(11),
PRIMARY KEY ("id")
);

-- ----------------------------
--  Records of "_videos"
-- ----------------------------
BEGIN;
INSERT INTO "_videos" VALUES (1, 'Testing Video', 25, 'Action', 'smb://192.168.0.2/ebahniptv/VOD/Action/SpiderMan2.mp4', null, null);
COMMIT;

-- ----------------------------
--  Table structure for "_vod"
-- ----------------------------
DROP TABLE IF EXISTS "_vod";
CREATE TABLE "_vod" (
"id"  INTEGER(11) NOT NULL,
"videoname"  TEXT(50),
"url"  TEXT(100),
"icon1"  TEXT(100),
"icon2"  TEXT(100),
"category"  TEXT(20),
"price"  INTEGER(11),
"watch"  INTEGER(11),
PRIMARY KEY ("id")
);

-- ----------------------------
--  Records of "_vod"
-- ----------------------------
BEGIN;
INSERT INTO "_vod" VALUES (1, 'Aashiqui ', 'smb://192.168.0.2/ebahniptv/VOD/Aashiqui 2.mp4', 'special://userdata/images/vod/test1.jpg', 'special://userdata/images/vod/test2.jpg', 'Action', 15, 5);
INSERT INTO "_vod" VALUES (2, 'Eagle Eye', 'smb://192.168.0.2/ebahniptv/VOD/Eagle.Eye.mp4', 'special://userdata/images/vod/test1.jpg', 'special://userdata/images/vod/test2.jpg', 'Action', 10, 0);
INSERT INTO "_vod" VALUES (3, 'My Name Is Khan', 'smb://192.168.0.2/ebahniptv/VOD/My.Name.Is.Khan.mp4', 'special://userdata/images/vod/test1.jpg', 'special://userdata/images/vod/test2.jpg', 'Action', 5, 0);
INSERT INTO "_vod" VALUES (4, 'The Mummy', 'smb://192.168.0.2/ebahniptv/VOD/Action/TheMummyResurrected.avi', 'special://userdata/images/vod/mummy1.jpg', 'special://userdata/images/vod/mummy2.jpg', 'Action', 10, 0);
INSERT INTO "_vod" VALUES (5, 'Krish 3', 'smb://192.168.0.2/ebahniptv/VOD/Action/Krrish3.mp4', 'special://userdata/images/vod/krish1.jpg', 'special://userdata/images/vod/krish2.jpg', 'Action', 25, 0);
INSERT INTO "_vod" VALUES (6, 'ArianaGrande', 'smb://192.168.0.2/ebahniptv/VOD/Music/ArianaGrande.mp4', 'special://userdata/images/vod/test1.jpg', 'special://userdata/images/vod/test2.jpg', 'Action', 10, 0);
INSERT INTO "_vod" VALUES (7, 'IggyAzalea', 'smb://192.168.0.2/ebahniptv/VOD/Music/IggyAzalea.mp4', 'special://userdata/images/vod/test1.jpg', 'special://userdata/images/vod/test2.jpg', 'Action', 10, 0);
INSERT INTO "_vod" VALUES (8, 'TaylorSwift', 'smb://192.168.0.2/ebahniptv/VOD/Music/TaylorSwift.mp4', 'special://userdata/images/vod/test1.jpg', 'special://userdata/images/vod/test2.jpg', 'Action', 10, 0);
INSERT INTO "_vod" VALUES (9, 'ArianaGrandeBreakFree', 'smb://192.168.0.2/ebahniptv/VOD/Music/ArianaGrandeBreakFree,mp4', 'special://userdata/images/vod/test1.jpg', 'special://userdata/images/vod/test2.jpg', 'Action', 10, 0);
INSERT INTO "_vod" VALUES (10, 'Eminem - Not Afraid', 'smb://192.168.0.2/ebahniptv/VOD/Music/Eminem - Not Afraid.mp4', 'special://userdata/images/vod/test1.jpg', 'special://userdata/images/vod/test2.jpg', 'Action', 10, 0);
INSERT INTO "_vod" VALUES (11, 'Akon - Smack That ft. Eminem', 'smb://192.168.0.2/ebahniptv/VOD/Music/Akon - Smack That ft. Eminem.mp4', 'special://userdata/images/vod/test1.jpg', 'special://userdata/images/vod/test2.jpg', 'Action', 10, 0);
INSERT INTO "_vod" VALUES (12, 'Akon - Bananza (Belly Dancer)', 'smb://192.168.0.2/ebahniptv/VOD/Music/Akon - Bananza (Belly Dancer).mp4', 'special://userdata/images/vod/test1.jpg', 'special://userdata/images/vod/test2.jpg', 'Action', 10, 0);
INSERT INTO "_vod" VALUES (13, 'Justin Bieber - Baby ft. Ludacris', 'smb://192.168.0.2/ebahniptv/VOD/Music/Justin Bieber - Baby ft. Ludacris.mp4', 'special://userdata/images/vod/test1.jpg', 'special://userdata/images/vod/test2.jpg', 'Action', 10, 0);
INSERT INTO "_vod" VALUES (14, 'Akon - Beautiful', 'smb://192.168.0.2/ebahniptv/VOD/Music/Akon - Beautiful.mp4', 'special://userdata/images/vod/test1.jpg', 'special://userdata/images/vod/test2.jpg', 'Action', 10, 0);
INSERT INTO "_vod" VALUES (15, 'Body Guard', 'smb://192.168.0.2/ebahniptv/VOD/Music/BodyGuard.mp4', 'special://userdata/images/vod/test1.jpg', 'special://userdata/images/vod/test2.jpg', 'Action', 10, 0);
COMMIT;

-- ----------------------------
--  Table structure for "_watched"
-- ----------------------------
DROP TABLE IF EXISTS "_watched";
CREATE TABLE "_watched" (
"id"  INTEGER(11) NOT NULL,
"ip"  TEXT(20),
"url"  TEXT,
"date"  TEXT,
PRIMARY KEY ("id")
);

-- ----------------------------
--  Records of "_watched"
-- ----------------------------
BEGIN;
INSERT INTO "_watched" VALUES (1, '192.168.0.5', 'smb://192.168.0.2/ebahniptv/VOD/Music/ArianaGrandeBreakFree,mp4', '2015-02-04 12:12:58');
INSERT INTO "_watched" VALUES (2, '192.168.0.5', 'smb://192.168.0.2/ebahniptv/VOD/Music/Eminem - Not Afraid.mp4', '2015-02-04 12:13:02');
INSERT INTO "_watched" VALUES (3, '192.168.0.5', 'smb://192.168.0.2/ebahniptv/VOD/Music/Akon - Bananza (Belly Dancer).mp4', '2015-02-04 12:13:02');
INSERT INTO "_watched" VALUES (4, '192.168.0.5', 'smb://192.168.0.2/ebahniptv/VOD/Music/Akon - Beautiful.mp4', '2015-02-04 12:13:03');
INSERT INTO "_watched" VALUES (5, '192.168.0.5', 'smb://192.168.0.2/ebahniptv/VOD/Music/TaylorSwift.mp4', '2015-02-04 12:13:03');
COMMIT;

-- ----------------------------
--  Table structure for "guestfolio"
-- ----------------------------
DROP TABLE IF EXISTS "guestfolio";
CREATE TABLE "guestfolio" (
"id"  INTEGER(10) NOT NULL,
"itemdesc"  TEXT(50) NOT NULL,
"itemamount"  TEXT(25) NOT NULL,
"itemdate"  TEXT(6),
"itemtime"  TEXT(6),
"roomnum"  TEXT(10) NOT NULL,
"acctnum"  TEXT(10) NOT NULL,
"balance"  TEXT(25),
PRIMARY KEY ("id")
);

-- ----------------------------
--  Records of "guestfolio"
-- ----------------------------
BEGIN;
INSERT INTO "guestfolio" VALUES (19, 'Room Charge', 20000, 120201, 150000, 1103, 123456, 47500);
INSERT INTO "guestfolio" VALUES (20, 'Mini Bar', 3100, 120201, 120100, 1103, 123456, 47500);
INSERT INTO "guestfolio" VALUES (21, 'Room Charge', 20000, 120202, 150000, 1103, 123456, 47500);
INSERT INTO "guestfolio" VALUES (22, 'Laundry', 2400, 120202, 140100, 1103, 123456, 47500);
INSERT INTO "guestfolio" VALUES (23, 'Broadband Access', 2000, 120202, 142300, 1103, 123456, 47500);
COMMIT;

-- ----------------------------
--  Table structure for "guestinfo"
-- ----------------------------
DROP TABLE IF EXISTS "guestinfo";
CREATE TABLE "guestinfo" (
"id"  INTEGER(10) NOT NULL,
"guestname"  TEXT(100) NOT NULL,
"lastname"  TEXT(100) NOT NULL,
"language"  TEXT(5),
"sflag"  TEXT(1),
"roomnum"  TEXT(10) NOT NULL,
"acctnum"  TEXT(10) NOT NULL,
"promocode"  TEXT(100) NOT NULL,
"chkodate"  TEXT,
PRIMARY KEY ("id")
);

-- ----------------------------
--  Records of "guestinfo"
-- ----------------------------
BEGIN;
INSERT INTO "guestinfo" VALUES (61086, 'Test', 'test', null, null, 201, '', 'n', null);
COMMIT;

-- ----------------------------
--  Table structure for "guestmessage"
-- ----------------------------
DROP TABLE IF EXISTS "guestmessage";
CREATE TABLE "guestmessage" (
"id"  INTEGER(10) NOT NULL,
"guestmsg"  TEXT(2000) NOT NULL,
"msgid"  TEXT(10) NOT NULL,
"msgdate"  TEXT(6),
"msgtime"  TEXT(6),
"roomnum"  TEXT(10) NOT NULL,
"acctnum"  TEXT(10) NOT NULL,
PRIMARY KEY ("id")
);

-- ----------------------------
--  Records of "guestmessage"
-- ----------------------------
BEGIN;
INSERT INTO "guestmessage" VALUES (1, 'Dear Mr Wong. <br>The dinner reservation you have requested had been processed.  <br>Please call back the operator to confirm.  <br>Thank you.', 111, 120201, 132300, 1103, 123456);
INSERT INTO "guestmessage" VALUES (2, 'Please call back the office urgently <br> message from Antechnic Office', 112, 120201, 191200, 1103, 123456);
INSERT INTO "guestmessage" VALUES (3, 'Package had been received in your behalf.  Please contact concierge to have it delivered to your room <br> from operator', 113, 120202, 175700, 1103, 123456);
COMMIT;

-- ----------------------------
--  Table structure for "mobile"
-- ----------------------------
DROP TABLE IF EXISTS "mobile";
CREATE TABLE "mobile" (
"id"  INTEGER(10) NOT NULL,
"description"  TEXT(100) NOT NULL,
"itemdate"  TEXT,
"roomnum"  TEXT(10) NOT NULL,
"status"  INTEGER(10) NOT NULL,
"officer"  TEXT(30),
PRIMARY KEY ("id")
);

-- ----------------------------
--  Records of "mobile"
-- ----------------------------
BEGIN;
INSERT INTO "mobile" VALUES (3, 'request for luggage pickup', '2012-04-09 00:00:00', 1101, 1, 0);
INSERT INTO "mobile" VALUES (4, 'testing', '2012-04-10 01:42:07', 1101, 1, 0);
INSERT INTO "mobile" VALUES (5, 'Make Up Room', '2012-04-10 01:55:18', 1103, 1, null);
INSERT INTO "mobile" VALUES (6, 'Taxi Service', '2012-04-10 02:13:10', 1103, 1, null);
INSERT INTO "mobile" VALUES (7, 'Luggage Pickup', '2012-04-10 02:55:58', 1103, 1, null);
INSERT INTO "mobile" VALUES (8, 'Taxi Service', '2012-04-11 14:14:05', 1103, 1, null);
INSERT INTO "mobile" VALUES (9, 'Luggage Pickup', '2012-04-11 14:16:07', 1103, 1, null);
INSERT INTO "mobile" VALUES (10, 'Make Up Room', '2012-04-11 14:16:48', 1103, 1, null);
INSERT INTO "mobile" VALUES (11, 'Laundry Pickup', '2012-04-11 14:17:35', 1103, 1, null);
INSERT INTO "mobile" VALUES (12, 'Taxi Service', '2012-04-11 14:18:55', 1103, 1, null);
INSERT INTO "mobile" VALUES (13, 'Laundry Pickup', '2012-04-11 14:23:03', 1103, 1, null);
INSERT INTO "mobile" VALUES (14, 'Luggage Pickup', '2012-04-11 14:23:49', 1103, 1, null);
INSERT INTO "mobile" VALUES (15, 'Make Up Room', '2012-04-11 14:30:38', 1103, 1, null);
INSERT INTO "mobile" VALUES (16, 'Taxi Service', '2012-04-11 17:17:56', 1103, 1, null);
INSERT INTO "mobile" VALUES (17, 'Make Up Room', '2012-04-11 17:26:03', 1103, 0, null);
INSERT INTO "mobile" VALUES (18, 'Laundry Pickup', '2012-05-17 16:26:36', 1103, 0, null);
INSERT INTO "mobile" VALUES (19, 'Laundry Pickup', '2012-05-22 17:02:48', 1103, 0, null);
INSERT INTO "mobile" VALUES (20, 'Laundry Pickup', '2012-06-27 15:43:48', 1103, 0, null);
INSERT INTO "mobile" VALUES (21, 'Make Up Room', '2012-07-22 17:32:06', 1103, 0, null);
COMMIT;

-- ----------------------------
--  Table structure for "pms_billing"
-- ----------------------------
DROP TABLE IF EXISTS "pms_billing";
CREATE TABLE "pms_billing" (
"id"  INTEGER(10) NOT NULL,
"room_no"  TEXT(5) NOT NULL,
"price"  TEXT(10) NOT NULL,
"post_time"  TEXT NOT NULL,
"status"  INTEGER(11) NOT NULL,
"transid"  TEXT(4),
PRIMARY KEY ("id")
);

PRAGMA foreign_keys = true;
