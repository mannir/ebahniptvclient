import xbmcgui
# Offline data

imglocation = 'special://userdata/images/menus/'
prop = {         
    'welcomeid': '1',
    'welcomena': 'welcome',
    'welcometi': 'Welcome Page',
    'welcomeic': imglocation+'main.jpg',
    'welcometh': imglocation+'main.jpg',
    'welcomeon': 'ActivateWindow(134)',
    'welcomevi': 'true',
    'mainid': '2',
    'mainna': 'main',
    'mainti': 'Main Page',
    'mainic': imglocation+'main.jpg',
    'mainth': imglocation+'main.jpg',
    'mainon': 'XBMC.RunScript(script.ebahniptv, main)',
    'mainvi': 'true',
    'tvid': '3',
    'tvna': 'tv',
    'tvti': 'TV Channels',
    'tvic': imglocation+'tv.jpg',
    'tvth': imglocation+'tv.jpg',
    'tvon': 'XBMC.RunScript(script.ebahniptv, tv)',
    'tvvi': 'true',
    'vodid': '4',
    'vodna': 'vod',
    'vodti': 'VOD',
    'vodic': imglocation+'vod.jpg',
    'vodth': imglocation+'vod.jpg',
    'vodon': 'XBMC.RunScript(script.ebahniptv, vod)',
    'vodvi': 'true',
    'hotelid': '5',
    'hotelna': 'hotel',
    'hotelti': 'Hotel Info',
    'hotelic': imglocation+'hotel.jpg',
    'hotelth': imglocation+'hotel.jpg',
    'hotelon': 'XBMC.RunScript(script.ebahniptv, hotel)',
    'hotelvi': 'true',
    'guestid': '6',
    'guestna': 'guest',
    'guestti': 'Guest Services',
    'guestic': imglocation+'guest.jpg',
    'guestth': imglocation+'guest.jpg',
    'gueston': 'XBMC.RunScript(script.ebahniptv, guest)',
    'guestvi': 'true',
    'entertainmentid': '7',
    'entertainmentna': 'entertainment',
    'entertainmentti': 'Entertainment',
    'entertainmentic': imglocation+'entertainment.jpg',
    'entertainmentth': imglocation+'entertainment.jpg',
    'entertainmenton': 'XBMC.RunScript(script.ebahniptv, entertainment)',
    'entertainmentvi': 'true',
    'interactivesid': '8',
    'interactivesna': 'interactives',
    'interactivesti': 'Interactives',
    'interactivesic': imglocation+'interactives.jpg',
    'interactivesth': imglocation+'interactives.jpg',
    'interactiveson': 'XBMC.RunScript(script.ebahniptv, interactives)',
    'interactivesvi': 'true',
    'weatherid': '9',
    'weatherna': 'weather',
    'weatherti': 'Weather',
    'weatheric': imglocation+'weather.jpg',
    'weatherth': imglocation+'weather.jpg',
    'weatheron': 'XBMC.RunScript(script.ebahniptv, weather)',
    'weathervi': 'true',
    'dsid': '10',
    'dsna': 'ds',
    'dsti': 'Digital Signage',
    'dsic': imglocation+'ds.jpg',
    'dsth': imglocation+'ds.jpg',
    'dson': 'XBMC.RunScript(script.ebahniptv, ds)',
    'dsvi': 'true',
    'helpid': '11',
    'helpna': 'help',
    'helpti': 'Remote Guide',
    'helpic': imglocation+'help.png',
    'helpth': imglocation+'help.png',
    'helpon': 'XBMC.RunScript(script.ebahniptv, help)',

    'hotel1pic': 'special://userdata/images/hotels/p1.jpg',
    'hotel2pic': 'special://userdata/images/hotels/p1.jpg',
    'hotel3pic': 'special://userdata/images/hotels/p1.jpg',
    'hotel4pic': 'special://userdata/images/hotels/p1.jpg',
    'hotel5pic': 'special://userdata/images/hotels/p1.jpg',
    'hotel6pic': 'special://userdata/images/hotels/p1.jpg',
    'hotel7pic': 'special://userdata/images/hotels/p1.jpg',
    'hotel1name': 'Sea-facing King Room',
    'hotel2name': 'Sea-facing Deluxe Twin',
    'hotel3name': 'Deluxe Twin Room',
    'hotel4name': 'Single Room',
    'hotel5name': 'Restaurant-The Cliff',
    'hotel6name': 'Restaurant',
 
    'roomno': '123',
    'guestname': 'Server Room',
    'hotel7name': 'Pool',
};



hotelinfo = {         
    'hotel1name': 'welcome',
};

vod = {
       'vodid': '1'
       
       
       }

# Create a list.
vod = []
vodurl = 'smb://192.168.0.2/ebahniptv/VOD/'
imgloc = 'special://userdata/images/vod/'

# Append empty lists in first two indexes.
for x in range(0, 10): vod.append([])

vod[0].append('1')
vod[0].append('Aashiqui')
vod[0].append(vodurl+'Aashiqui 2.mp4')
vod[0].append(imgloc+'vod1a.jpg')
vod[0].append(imgloc+'vod1b.jpg')
vod[0].append('Action')
vod[0].append('15')
vod[0].append('5')

vod[1].append('2')
vod[1].append('TaylorSwift')
vod[1].append(vodurl+'TaylorSwift.mp4')
vod[1].append(imgloc+'vod2a.jpg')
vod[1].append(imgloc+'vod2b.jpg')
vod[1].append('Action')
vod[1].append('15')
vod[1].append('5')

vod[2].append('3')
vod[2].append('Krish 3')
vod[2].append(vodurl+'TaylorSwift.mp4')
vod[2].append(imgloc+'vod3a.jpg')
vod[2].append(imgloc+'vod3b.jpg')
vod[2].append('Action')
vod[2].append('15')
vod[2].append('5')

vod[3].append('4')
vod[3].append('TaylorSwift')
vod[3].append(vodurl+'TaylorSwift.mp4')
vod[3].append(imgloc+'vod4a.jpg')
vod[3].append(imgloc+'vod4b.jpg')
vod[3].append('Action')
vod[3].append('15')
vod[3].append('5')

vod[4].append('5')
vod[4].append('TaylorSwift')
vod[4].append(vodurl+'TaylorSwift.mp4')
vod[4].append(imgloc+'vod5a.jpg')
vod[4].append(imgloc+'vod5b.jpg')
vod[4].append('Action')
vod[4].append('15')
vod[4].append('5')

# Display top-left element.
#print(vod[1][2])

# Display entire list.
#print(vod)
'''
# Loop over rows.
for row in vod:
    # Loop over columns.
    for column in row:
        print(column)
        print("\n")
'''

###############################################################################
for k,v in prop.items():
    print k+"=="+v
    try: xbmcgui.Window(10000).setProperty(k, v)
    except: print 