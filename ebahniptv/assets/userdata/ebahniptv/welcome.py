import xbmc, xbmcgui
 
#get actioncodes from https://github.com/xbmc/xbmc/blob/master/xbmc/guilib/Key.h
ACTION_PREVIOUS_MENU = 10 # Esc
ACTION_NAV_BACK = 92 # Backspace

ALIGN_CENTER = 6

background_img = 'special://userdata/images/welcome.jpg'
button_nf_img = 'special://userdata/images/eng1.ico'
button_fo_img = 'special://userdata/images/chi1.png'
banana_img = 'special://userdata/images/gl.png'
 
class MyClass(xbmcgui.Window):
    def __init__(self):
        self.addControl(xbmcgui.ControlImage(0,0, xbmcgui.Window.getWidth(self) ,xbmcgui.Window.getHeight(self),
                                         'special://userdata/images/welcome.jpg'))
    
        self.addControl(xbmcgui.ControlImage(20, 20, 150, 150, 'special://userdata/images/logo1.png'))
    
    

        self.strActionInfo = xbmcgui.ControlLabel(350, 30, 1000, 100, 'Welcome to eBahn IPTV Solutions', 'cirrus_40_Bold')
        self.addControl(self.strActionInfo)
        self.strActionInfo.setLabel('Welcome to eBahn IPTV Solutions')
    
        self.addControl(xbmcgui.ControlImage(270, 130, 750, 300, 'special://userdata/images/gl.png'))
    
        self.strActionInfo = xbmcgui.ControlLabel(300, 150, 1000, 500, 'Lebel1', 'cirrus_20_Bold')
        self.addControl(self.strActionInfo)
         
    
    #self.strActionFade = xbmcgui.ControlFadeLabel(200, 300, 500, 300, 'font13', '0xFFFFFF00')
    #self.addControl(self.strActionFade)
    #self.strActionFade.addLabel('This is a fade label')
    
        self.paneItemText = xbmcgui.ControlTextBox(500,200,580,440,"font11","0xffF4F4F4")#x,y,xd,yd
        self.addControl(self.paneItemText)
    
    
        self.privet_btn = xbmcgui.ControlButton(530, 550, 100, 100, u'English', focusTexture='special://userdata/images/eng2.png', 
                                            noFocusTexture='special://userdata/images/eng1.ico', alignment=6)
        self.addControl(self.privet_btn)
    
        self.exit_btn = xbmcgui.ControlButton(680, 550, 100, 100, u'Chinese', focusTexture='special://userdata/images/chi2.png', 
                                          noFocusTexture='special://userdata/images/chi1.png', alignment=6)
        self.addControl(self.exit_btn)
    
    
    #self.set_controls()
        self.set_navigation()
    
    #self.setFocus(self.privet_btn)
    
    
    
    

    def set_navigation(self):
        self.privet_btn.controlRight(self.exit_btn)
        self.privet_btn.controlLeft(self.exit_btn)
        self.exit_btn.controlRight(self.privet_btn)
        self.exit_btn.controlLeft(self.privet_btn)
        self.setFocus(self.privet_btn)

    def onAction(self, action):
        if action == ACTION_NAV_BACK or action == ACTION_PREVIOUS_MENU:
            self.close()
            #xbmc.executebuiltin('ActivateWindow(4444)')

    def onControl(self, control):
        if control == self.privet_btn:
            #self.dialog.ok(u'eBahn IPTV', u'Thank you :-)')
            self.close()
            #xbmc.executebuiltin('ActivateWindow(4444)')
        elif control == self.exit_btn:
            self.close()
            #xbmc.executebuiltin('ActivateWindow(4444)') 
    
    

if __name__ == '__main__':
    addon = MyClass()
    addon.doModal()
    del addon
