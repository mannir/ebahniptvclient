import os, sys, re, socket, datetime, time
import xbmc, xbmcgui, xbmcaddon, xbmcplugin
from default import IPTV
from default import db

iptv = IPTV()

d = {}

xaxis = ["190", "380", "570"]
yaxis = ['150','300','450','600','750']
'''
id=0
for x in xaxis:
    for y in yaxis:
        id += 1
        print 'x='+x+',y='+str(y),'id='+str(id),
    print "\n"

'''


ACTION_PREVIOUS_MENU = 10 # Esc
ACTION_NAV_BACK = 92 # Backspace
ALIGN_CENTER = 6
img = 'special://userdata/images/'
vod = iptv.vod


#for i in range(0,5): print vod[i][3]


background_img = 'special://userdata/images/menus/vod.jpg'
button_nf_img = 'button-nofocus.png'
button_fo_img = 'button-focus.png'
font = 'cirrus_20'
font2 = 'cirrus_40_Bold'
ip = xbmc.getInfoLabel(xbmc.getIPAddress())
font = 'cirrus_16'
roomno = 0;
guestname ='';

class MyAddon(xbmcgui.WindowDialog):
    def __init__(self):
        self.dialog = xbmcgui.Dialog()
        self.dashboard()   

    def onAction(self, action):
        if action == ACTION_NAV_BACK or action == ACTION_PREVIOUS_MENU:	    
	    self.close()
        
    def onFocus(self, control):
        if control == d['ch1']:
            xbmcgui.Dialog().ok("eBahnIPTV", "ch1")
            
    def playVod(self, videoname):
        vn = videoname.split('/')
        '''
        try:
            sql.execute("SELECT * FROM `_watched` where ip = '"+xbmc.getIPAddress()+"' AND video = '"+str(vn[6])+"'")
            row = sql.fetchone ()
            videoname2=  str(row[1])
        except e: xbmc.log("eBahnIPTV1", e)
        '''
        #xbmcgui.Dialog().ok("eBahnIPTV", "hello")
        '''
        vname = ''
        try:
            
            vname = str(vn[6])
        except: print 'hi'
        
        
        
        '''
        '''
        try:
            sql.executemany("INSERT INTO _watched(ip,url) VALUES (%s,%s)", ( (xbmc.getIPAddress(), str(vn[6])), ))
            db.commit();
        except Exception, e: xbmc.log("eBahnIPTV", e)
        
        
    '''
        '''
        
        '''
       ## xbmcgui.Dialog().ok(videoname);
        '''
        price = 15
        response = urllib.urlopen(serveraddr + "json?ip="+xbmc.getIPAddress()+"&video="+videoname);
        rsp = json.loads(response.read())
        
        if(rsp=='1'):
            time.sleep(30)
            xbmc.Player().pause()
            ret = xbmcgui.Dialog().yesno("eBahn VOD Subscription", "Movie Price: "+str(price)+" SGD", "Click 'Yes' to start watching.The rate above will be charged to your room bill.",'', "No", "Yes")
            
            if ret==1:
                xbmcgui.Dialog().ok("eBahn IPTV", str(price) + " (SGD) has been charged to your room bill. Thank you: ")
                response = urllib.urlopen(serveraddr + "json?type=purchase&ip=192.168.0.5&video=test.mp4&amount="+price);
                rsp = json.loads(response.read())
        
                xbmc.Player().play(videoname)
                    #xbmc.executebuiltin('XBMC.PlayerControl(Play)') 
            else: xbmc.Player().stop();
                
            xbmcgui.Dialog().ok("eBahnIPTV", "Buy now")
        else:
            xbmcgui.Dialog().ok('eBahn IPTV', 'Already Purchased. Enjoy watching')
        '''
        self.close()
        xbmc.Player().play(videoname)
        #xbmc.Player().play(videoname)
        
    def dashboard(self):
        self.addControl(xbmcgui.ControlImage(170, 70, 1000, 550, 'special://userdata/images/contentpanel.png'))
        ##self.addControl(xbmcgui.ControlImage(0, 0, self.getWidth(), self.getHeight(), 'special://userdata/images/bg.jpg'))
        self.addControl(xbmcgui.ControlImage(450, 100, 700, 500, 'special://userdata/images/vod/vodbg.jpg'))
        x1=200
        d['ch1']  = xbmcgui.ControlButton(x1, 100, 200, 50,'Action Movies', focusTexture=img+'icons/btn.png', noFocusTexture=img+'icons/btn2.png', alignment=2)
        d['ch2']  = xbmcgui.ControlButton(x1, 200, 200, 50,'Horror Movies', focusTexture=img+'icons/btn.png', noFocusTexture=img+'icons/btn2.png', alignment=2)
        d['ch3']  = xbmcgui.ControlButton(x1, 300, 200, 50,'Comedy Movies', focusTexture=img+'icons/btn.png', noFocusTexture=img+'icons/btn2.png', alignment=2)
        d['ch4']  = xbmcgui.ControlButton(x1, 400, 200, 50,'Drama Movies', focusTexture=img+'icons/btn.png', noFocusTexture=img+'icons/btn2.png', alignment=2)
        d['ch5']  = xbmcgui.ControlButton(x1, 500, 200, 50,'Kids Movies', focusTexture=img+'icons/btn.png', noFocusTexture=img+'icons/btn2.png', alignment=2)
        
        self.addControl(d['ch1'] )
        self.addControl(d['ch2'] )
        self.addControl(d['ch3'] )
        self.addControl(d['ch4'] )
        self.addControl(d['ch5'] )
        #self.addControl(xbmcgui.ControlLabel(400, 100, 1000, 50, 'eBahn IPTV TV Channels',font2))
     
        d["ch1"].controlDown(d["ch2"])
        d["ch2"].controlDown(d["ch3"])
        d["ch3"].controlDown(d["ch4"])
        d["ch4"].controlDown(d["ch5"])
        d["ch5"].controlDown(d["ch1"])
        
        d["ch1"].controlUp(d["ch5"])
        d["ch2"].controlUp(d["ch1"])
        d["ch3"].controlUp(d["ch2"])
        d["ch4"].controlUp(d["ch3"])
        d["ch5"].controlUp(d["ch4"])
        
        self.setFocus(d['ch1'])
        
    def videos(self, category):
        self.addControl(xbmcgui.ControlImage(0, 0, self.getWidth(), self.getHeight(), 'special://userdata/images/bg.jpg'))
        #self.addControl(xbmcgui.ControlImage(20, 100, 1200, 500, 'special://skin/backgrounds/games.jpg'))
        self.addControl(xbmcgui.ControlLabel(350, 50, 1000, 50, 'IPTV: Action Movies Categories',font2))
        #x=190
        #y=150
        xaxis = [190, 380, 570,760,950]
        yaxis = [150,300,450,600]                
        id=0
        for y in yaxis:
            for x in xaxis:
                id += 1
                d['v'+str(id)] = xbmcgui.ControlButton(x, y, 150, 100, 'Video '+str(id), focusTexture=img+'vod/vod1a.jpg', noFocusTexture=img+'vod/vod1b.jpg',alignment=6,textColor='ff00ff7f')
                self.addControl(d['v'+str(id)])

        self.setFocus(d['v1'])
        
        for id in range(1,id):
            d['v'+str(id)].controlRight(d['v'+str(id+1)])
            d['v'+str(id)].controlDown(d['v'+str(id+1)])
            if(id>1):
                d['v'+str(id)].controlLeft(d['v'+str(id-1)])
                d['v'+str(id)].controlUp(d['v'+str(id-1)])
        
        d['v20'].controlRight(d['v1'])
        d['v20'].controlDown(d['v1'])
        d['v20'].controlLeft(d['v19'])
        d['v20'].controlUp(d['v19'])

               
    def onControl(self, control):
        if control == d['ch1']:
            #self.close()
            self.videos('Action')
        elif control == d['ch2']: xbmcgui.Dialog().ok("IPTV", "This Category has not been added!, sorry for inconvenience!")
        elif control == d['ch3']: xbmcgui.Dialog().ok("IPTV", "This Category has not been added!, sorry for inconvenience!")
        elif control == d['ch4']: xbmcgui.Dialog().ok("IPTV", "This Category has not been added!, sorry for inconvenience!")
        elif control == d['ch5']: xbmcgui.Dialog().ok("IPTV", "This Category has not been added!, sorry for inconvenience!")
        
        elif control == d['v1']:
            xbmcgui.Dialog().ok("IPTV", "Play Video 1")
            self.playVod(vod[0][2])     
        elif control == d['v2']: self.playVod(vod[1][2])
        elif control == d['v3']: self.playVod(vod[2][2])
        elif control == d['v4']: self.playVod(vod[3][2])     
        elif control == d['v5']: self.playVod(vod[4][2])
        elif control == d['v6']: self.playVod(vod[5][2])
        elif control == d['v7']: self.playVod(vod[6][2])
        elif control == d['v8']: self.playVod(vod[7][2])
        elif control == d['v9']: self.playVod(vod[8][2])
        elif control == d['v10']: self.playVod(vod[9][2])
        elif control == d['v11']: self.playVod(vod[10][2])
        elif control == d['v12']: self.playVod(vod[11][2])
        elif control == d['v13']: self.playVod(vod[12][2])
        elif control == d['v14']: self.playVod(vod[13][2])
        elif control == d['v15']: self.playVod(vod[14][2])


        else: xbmcgui.Dialog().ok("eBahnIPTV", "Video is not Available now!, sorry for inconvenience!")
            
            ###################################################################################
        '''
            videoname = vd[1][1]
            price = 30
            try:
                sql.execute("SELECT url FROM _watched WHERE ip='"+xbmc.getIPAddress()+"'")
                videoname2 = str(sql.fetchone()[0])
                xbmcgui.Dialog().ok("eBahnIPTV", "v1="+videoname)
                xbmcgui.Dialog().ok("eBahnIPTV", "v2="+videoname2)
            except: print 'hi'
            
            
            
            if(videoname!=videoname2):
                time.sleep(30)
                xbmc.Player().pause()
                ret = xbmcgui.Dialog().yesno("eBahn VOD Subscription", "Movie Price: "+str(price)+" SGD", "Click 'Yes' to start watching.The rate above will be charged to your room bill.",'', "No", "Yes")
                
                if ret==1:
                    xbmcgui.Dialog().ok("eBahn IPTV", str(price) + " (SGD) has been charged to your room bill. Thank you: ")
                    sql.executemany("INSERT INTO _watched(ip,url) VALUES (%s,%s)", ( (xbmc.getIPAddress(), videoname), ))
                    warnings = sql.fetchwarnings()
                    if warnings: xbmc.log("eBahnIPTV: " + str(warnings))
                    xbmc.Player().play(vd[1][1])
                    #xbmc.executebuiltin('XBMC.PlayerControl(Play)') 
                else: xbmc.Player().stop();
                
            else: xbmcgui.Dialog().ok('eBahn IPTV', 'Already Purchased. Enjoy watching')
        '''
 #####################################################################################################         
            
        
            #self.close()
            #xbmc.Player().play(vd[0][1])
        #elif control == d['ch2']: xbmc.executebuiltin('XBMC.StartAndroidActivity("com.android.calculator2")')

if __name__ == '__main__':
    addon = MyAddon()
    addon.doModal()
    del addon
