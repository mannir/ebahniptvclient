import os, sys
import xbmc, xbmcgui, xbmcaddon


ACTION_PREVIOUS_MENU = 10 # Esc
ACTION_NAV_BACK = 92 # Backspace

ALIGN_CENTER = 6

p1 = sys.argv[1]
background_img = p1
button_nf_img = 'button-nofocus.png'
button_fo_img = 'button-focus.png'
#banana_img = 'special://userdata/images/tvchannels.jpg'
#xbmc.executebuiltin('Notification(WELCOME MANNIR,'+str(background_img)+',1000,/logo.gif)')

class MyAddon(xbmcgui.WindowDialog):

    def __init__(self):
        self.dialog = xbmcgui.Dialog()
        background = xbmcgui.ControlImage(0, 0, xbmcgui.Window.getWidth(self), xbmcgui.Window.getHeight(self), background_img)
        self.addControl(background)
        #banana_picture = xbmcgui.ControlImage(500, 200, 256, 256, banana_img)
        #self.addControl(banana_picture)
        self.set_controls()
        self.set_navigation()

    def set_controls(self):
        self.privet_btn = xbmcgui.ControlButton(10, 10, 110, 40, u'Back', focusTexture=button_fo_img,
                                                        noFocusTexture=button_nf_img, alignment=ALIGN_CENTER)
        self.addControl(self.privet_btn)
        #self.exit_btn = xbmcgui.ControlButton(650, 500, 110, 40, u'Previous', focusTexture=button_fo_img,
        #                                                noFocusTexture=button_nf_img, alignment=ALIGN_CENTER)
        #self.addControl(self.exit_btn)

    def set_navigation(self):
        #self.privet_btn.controlRight(self.exit_btn)
        #self.privet_btn.controlLeft(self.exit_btn)
        #self.exit_btn.controlRight(self.privet_btn)
        #self.exit_btn.controlLeft(self.privet_btn)
        self.setFocus(self.privet_btn)

    def onAction(self, action):
        if action == ACTION_NAV_BACK or action == ACTION_PREVIOUS_MENU:
            self.close()
            xbmc.executebuiltin('ActivateWindow(4444)')

    def onControl(self, control):
        if control == self.privet_btn:
            #self.dialog.ok(u'eBahn IPTV', u'Thank you :-)')
            self.close()
            xbmc.executebuiltin('ActivateWindow(4444)')
        elif control == self.exit_btn:
            self.close()
            xbmc.executebuiltin('ActivateWindow(4444)')

if __name__ == '__main__':
    addon = MyAddon()
    addon.doModal()
    del addon
