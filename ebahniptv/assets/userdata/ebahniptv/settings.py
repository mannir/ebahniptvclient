import urllib
import urllib2
import socket
import os, sys, re
import datetime
import xbmcplugin
import xbmcgui
import xbmcaddon
import xbmc
from default import IPTV
iptv = IPTV()
b = {}


msg = ['','I need Loundry','Room Services','Electricity not Working','No Water Available','Telephone not Working']
msg2 = ['','I need Loundry','Room Services','Electricity not Working','No Water Available','Telephone not Working']
msgtype = {'msg1': 'I need Loundry','msg2': 'Room Services','msg3': 'Electricity not Working','msg4': 'No Water Available','msg5':'Need Food'}
#msgtype = {'name': 'value'};

ACTION_PREVIOUS_MENU = 10 # Esc
ACTION_NAV_BACK = 92 # Backspace

ALIGN_CENTER = 6

img = 'special://userdata/images/icons/'

newmsg1 = img+'newmsg2.png';
newmsg2 = img+'newmsg1.png';
readmsg1 = img+'readmsg2.png';
readmsg2 = img+'readmsg1.png';
gmsg1 = img+'gmsg2.png';
gmsg2 = img+'gmsg1.png';
msgtemp1 = img+'msgtemp2.png';
msgtemp2 = img+'msgtemp1.png';


msg_btn = img+'message.png';
send_btn = img+'sendmsg.png';
background_img = 'special://userdata/images/menus/guest.jpg'
button_nf_img = 'button-nofocus.png'
button_fo_img = 'button-focus.png'
ip = xbmc.getInfoLabel(xbmc.getIPAddress())
#xbmc.executebuiltin('Notification(eBahn IPTV,'+str(p1)+',1000,/logo.gif)')
font = 'cirrus_20'
font2 = 'cirrus_40_Bold'
roomno = 0;
guestname ='';
myip = xbmc.getInfoLabel(xbmc.getIPAddress())

class MyAddon(xbmcgui.WindowDialog):
    def __init__(self):
        ##self.dashboard()
        self.enterPin()
        
        #self.set_controls()
        #self.set_navigation()

    def set_controls(self):
        self.privet_btn = xbmcgui.ControlButton(10, 10, 110, 40, u'Back', focusTexture=button_fo_img,
                                                        noFocusTexture=button_nf_img, alignment=ALIGN_CENTER)
        self.addControl(self.privet_btn)
        #self.exit_btn = xbmcgui.ControlButton(650, 500, 110, 40, u'Previous', focusTexture=button_fo_img,
        #                                                noFocusTexture=button_nf_img, alignment=ALIGN_CENTER)
        #self.addControl(self.exit_btn)

    def set_navigation(self):
        print ''
        #self.privet_btn.controlRight(self.exit_btn)
        #self.privet_btn.controlLeft(self.exit_btn)
        #self.exit_btn.controlRight(self.privet_btn)
        #self.exit_btn.controlLeft(self.privet_btn)
        #self.sendmsg.controlLeft(self.viewmsg)
        #self.sendmsg.controlRight(self.viewmsg)
        #self.viewmsg.controlLeft(self.sendmsg)
        #self.viewmsg.controlRight(self.sendmsg)
        #self.gmsg.controlLeft(self.viewmsg)
        #self.gmsg.controlRight(self.msgtemp)
        #self.msgtemp.controlLeft(self.viewmsg)
        #self.msgtemp.controlRight(self.sendmsg)
               
        #self.setFocus(self.sendmsg)

    def onAction(self, action):
        if action == ACTION_NAV_BACK or action == ACTION_PREVIOUS_MENU:
            self.close()
            #xbmc.executebuiltin('ActivateWindow(4444)')

    def onControl(self, control):
       # if control == self.msgtemp:
            #self.msgtemp()
        if control == self.sendmsg:
            self.messagetype()
            #self.messages()
            xbmc.executebuiltin('XBMC.StartAndroidActivity("com.mannir.ebahn2")')
        if control == self.viewmsg:
            self.listmessage()
        #if control == self.gmsg: xbmc.executebuiltin('XBMC.StartAndroidActivity("com.mannir.ebahn2")') # self.groupmsg()
        
        #xbmc.executebuiltin('Notification(WELCOME MANNIR,'+str(control)+',1000,/logo.gif)')
        if control == b['b1'] :
            xbmcgui.Dialog().ok('IPTV', 'Message Sent!')
            iptv.sendMessage(iptv.ip, '192.168.0.1', msg[1])
            xbmcgui.Dialog().ok('IPTV', 'Message Sent!')
        if control == b['b2'] :
            iptv.sendMessage(iptv.ip, '192.168.0.1', msg[2])
            xbmcgui.Dialog().ok('IPTV', 'Message Sent!')
        if control == b['b3'] :
            iptv.sendMessage(iptv.ip, '192.168.0.1', msg[3])
            xbmcgui.Dialog().ok('IPTV', 'Message Sent!')

        if control == b['b4'] :
            iptv.sendMessage(iptv.ip, '192.168.0.1', msg[4])
            xbmcgui.Dialog().ok('IPTV', 'Message Sent!')

        if control == b['b5'] :
            iptv.sendMessage(iptv.ip, '192.168.0.1', msg[5])
            xbmcgui.Dialog().ok('IPTV', 'Message Sent!')


        if control == self.loundry_btn:
            kb = xbmc.Keyboard('default', 'heading')
            kb.setDefault('I need Laundry now!') # optional
            kb.setHeading('Enter Message') # optional
        #kb.setHiddenInput(True) # optional
            kb.doModal()
            if (kb.isConfirmed()):
                text = kb.getText()
                xbmc.executebuiltin('Notification(eBahnIPTV,'+text+',1000,/logo.gif)')
            else: self.dashboard()
            #self.dialog.ok(u'eBahn IPTV', u'Thank you :-)')
            #self.close()
            #xbmc.executebuiltin('ActivateWindow(4444)')
        elif control == self.exit_btn:
            self.close()
            #xbmc.executebuiltin('ActivateWindow(4444)')

        else: self.close()
    
    def makeWindow(self):

        headline = xbmcgui.ControlLabel(10,20,300,50,"Recordings on mannir","font14")
        self.addControl(headline)

        self.list = xbmcgui.ControlList(500,300, self.getWidth()-100, self.getHeight()-80)
        self.list.setPageControlVisible(True)
        self.addControl(self.list)
        self.setFocus(self.list)
        
    def dashboard(self):
        self.addControl(xbmcgui.ControlImage(350, 100, 800, 500, 'ContentPanel.png'))
        #self.addControl(xbmcgui.ControlImage(0, 0, self.getWidth(), self.getHeight(), 'special://userdata/images/bg.jpg'))
        #self.addControl(xbmcgui.ControlImage(0, 0, self.getWidth(), self.getHeight(), 'special://skin/backgrounds/bg/vod.jpg'))
        self.addControl(xbmcgui.ControlLabel(550, 120, 1000, 50, 'System Settings',font2))
        
        self.sendmsg = xbmcgui.ControlButton(500, 200, 150, 100,'', focusTexture=newmsg2, noFocusTexture=newmsg1)
        self.addControl(self.sendmsg)
        self.addControl(xbmcgui.ControlLabel(500, 300, 500, 50, 'Send Message', 'cirrus_20_Bold'))
    
        self.viewmsg = xbmcgui.ControlButton(800, 200, 150, 100,'', focusTexture=readmsg2, noFocusTexture=readmsg1)
        self.addControl(self.viewmsg)
        self.addControl(xbmcgui.ControlLabel(800, 300, 500, 50, 'View Message', 'cirrus_20_Bold'))
        
        #self.gmsg = xbmcgui.ControlButton(700, 200, 200, 150, u'Group Message', focusTexture=gmsg2, noFocusTexture=gmsg1,alignment=6)
        #self.addControl(self.gmsg)
        '''
        self.msgtemp = xbmcgui.ControlButton(800, 200, 200, 150, u'Message Templates', focusTexture=msgtemp2, noFocusTexture=msgtemp1,alignment=6)
        self.addControl(self.msgtemp)
        self.addControl(xbmcgui.ControlLabel(440, 400, 500, 50, 'Guest Folio', 'cirrus_20_Bold'))
        ''' 
        self.setFocus(self.sendmsg)      
        
    def messages(self):
        kb = xbmc.Keyboard('default', 'heading')
        kb.setDefault('') # optional
        kb.setHeading('Enter Room No (Press OK to cancel)') # optional
        #kb.setHiddenInput(True) # optional
        kb.doModal()
        if (kb.getText()!=''):
            ip = '192.168.0.' + kb.getText()
            kb = xbmc.Keyboard('default', 'heading')
            kb.setDefault('I need Laundry now!') # optional
            kb.setHeading('Enter Message to '+str(ip)+' press OK to cancel or Send') # optional
        #kb.setHiddenInput(True) # optional
            kb.doModal()
            if (kb.isConfirmed()):
                message = kb.getText()
                                
                msg = message.replace(' ', '%20')
                url = "%7B%22jsonrpc%22%3A%222.0%22%2C%22method%22%3A%22GUI.ShowNotification%22%2C%22params%22%3A%7B%22title%22%3A%22Message%20from%20"+myip+"%22%2C%22message%22%3A%22"+msg+"%22%7D%2C%22id%22%3A1%7D%22)"
                status = urllib2.urlopen('http://'+ip+':8080/jsonrpc?request='+url).read()
                xbmc.log("eBahnIPTV: "+status)
                #client.GUI.ActivateWindow({"window":"weather"})
                iptv.sendMessage(myip, ip, message)
                xbmc.executebuiltin('Notification(eBahnIPTV,Message sent to '+ip+',10000,/logo.gif)')
            #else:
                #self.close()
                #self.dashboard()
       # else:
            #self.close()
            #self.dashboard()
                
    def listmessage(self):
        self.dialog = xbmcgui.Dialog()
        
        background = xbmcgui.ControlImage(0, 0, self.getWidth(), self.getHeight(), background_img)
        self.addControl(background)

        self.addControl(xbmcgui.ControlImage(10, 50, 1260, 600, 'ContentPanel.png'))
    
        self.addControl(xbmcgui.ControlLabel(50, 70, 300, 50, 'Room No: '+ str(iptv.roomno), font))
        self.addControl(xbmcgui.ControlLabel(50, 90, 300, 50, 'Guest Name: '+ str(iptv.guestname), font))
        self.addControl(xbmcgui.ControlLabel(50, 110, 300, 50, 'IP Address: '+ xbmc.getInfoLabel(xbmc.getIPAddress()), font))
        
        #y = [100, 200, 300, 400, 500]
        #for index in range(len(y)):
        y = 150;
        self.addControl(xbmcgui.ControlLabel(50, y, 200, 50, 'No', font))
        self.addControl(xbmcgui.ControlLabel(100, y, 300, 50, 'Date', font))
        self.addControl(xbmcgui.ControlLabel(300, y, 500, 50, 'From', font))
        self.addControl(xbmcgui.ControlLabel(500, y, 200, 50, 'To', font))
        self.addControl(xbmcgui.ControlLabel(700, y, 1000, 50, 'Message Details', font))

        y = 180
        while (y <= 700):
            self.addControl(xbmcgui.ControlImage(0, y, 1280, 20, 'menuitemNF.png'))
            y = y +20
            
        y2 = 180
        sn = 1
        for row in iptv.getMessages(ip):
            if((row[0])<=20):
                id = str(row[0])
                fr = str(row[1])
                to = str(row[2])
                msg = str(row[3])
                date = str(row[4])
                #videoname = str(row[2])
                #videoname = str(row[2])[0:50]
                #ch = str(row[1])
                #pic = str(row[3])
                #url = 
                self.addControl(xbmcgui.ControlLabel(50, y2, 300, 20, id, font))
                self.addControl(xbmcgui.ControlLabel(100, y2, 200, 50, date, font))
                self.addControl(xbmcgui.ControlLabel(300, y2, 500, 50, fr, font))
                self.addControl(xbmcgui.ControlLabel(500, y2, 200, 50, to, font))
                self.addControl(xbmcgui.ControlLabel(700, y2, 1000, 50, msg,font))
                y2 = y2 + 20
            #addDir(ch, url, 'playVideo', pic)
    #self.updateRecordings()
        
    def messagetype(self):
        self.addControl(xbmcgui.ControlImage(50, 100, 1200, 500, 'ContentPanel.png'))
        
        y = 200
        for i in range(1,6):
            b['b'+str(i)] = xbmcgui.ControlButton(500, y, 400, 50, msg[i], focusTexture=button_fo_img,noFocusTexture=button_nf_img, alignment=ALIGN_CENTER)
            self.addControl(b['b'+str(i)])
            print 'mm1:'+'b'+str(i)+"=="+msg[i]
            y =y + 50       
        self.setFocus(b['b1'])
        
        for i in range(1,5):
            b['b'+str(i)].controlRight(b['b'+str(i+1)])
            b['b'+str(i)].controlDown(b['b'+str(i+1)])
            if(i>1):
                b['b'+str(i)].controlLeft(b['b'+str(i-1)])
                b['b'+str(i)].controlUp(b['b'+str(i-1)])
            
        b['b5'].controlRight(b['b1'])
        b['b5'].controlDown(b['b1'])
        b['b5'].controlLeft(b['b4'])
        b['b5'].controlUp(b['b4'])
        
    def messagetype2(self):
        self.addControl(xbmcgui.ControlImage(50, 100, 1200, 500, 'ContentPanel.png'))
        self.addControl(xbmcgui.ControlLabel(400, 120, 1000, 50, 'House Keeping Message Type',font2))
        
        y = 200
        for i in range(1,6):
            b['b'+str(i)] = xbmcgui.ControlButton(500, y, 400, 50, msg2[i], focusTexture=button_fo_img,noFocusTexture=button_nf_img, alignment=ALIGN_CENTER)
            self.addControl(b['b'+str(i)])
            print 'mm1:'+'b'+str(i)+"=="+msg2[i]
            y =y + 50       
        self.setFocus(b['b1'])
        
        for i in range(1,5):
            b['b'+str(i)].controlRight(b['b'+str(i+1)])
            b['b'+str(i)].controlDown(b['b'+str(i+1)])
            if(i>1):
                b['b'+str(i)].controlLeft(b['b'+str(i-1)])
                b['b'+str(i)].controlUp(b['b'+str(i-1)])
            
        b['b5'].controlRight(b['b1'])
        b['b5'].controlDown(b['b1'])
        b['b5'].controlLeft(b['b4'])
        b['b5'].controlUp(b['b4'])
        
    def msgtemp(self):
        self.addControl(xbmcgui.ControlImage(50, 100, 1200, 500, 'ContentPanel.png'))
        self.addControl(xbmcgui.ControlLabel(200, 120, 1000, 50, 'eBahn IPTV Interactives Services: Message Templates',font2))
        
    def groupmsg(self):
        kb = xbmc.Keyboard('default', 'heading')
        kb.setDefault('') # optional
        kb.setHeading('Enter Room No separate by space! (to cancel leave blank and Press OK)') # optional
        #kb.setHiddenInput(True) # optional
        kb.doModal()
        if (kb.getText()!=''):
            ip = '192.168.0.' + kb.getText()
            kb = xbmc.Keyboard('default', 'heading')
            kb.setDefault('we have to goto lunch now!') # optional
            kb.setHeading('Enter Message to groups (to cancel leav blacnk and press OK)') # optional
        #kb.setHiddenInput(True) # optional
            kb.doModal()
            if (kb.isConfirmed()):
                xbmc.executebuiltin('Notification(eBahnIPTV,Message sent to groups thank you for using eBahnIPTV,10000,/logo.gif)')
                message = kb.getText()
                                
                msg = message.replace(' ', '%20')
                url = "%7B%22jsonrpc%22%3A%222.0%22%2C%22method%22%3A%22GUI.ShowNotification%22%2C%22params%22%3A%7B%22title%22%3A%22Message%20from%20"+myip+"%22%2C%22message%22%3A%22"+msg+"%22%7D%2C%22id%22%3A1%7D%22)"
                status = urllib2.urlopen('http://'+ip+':8080/jsonrpc?request='+url).read()
                xbmc.log("eBahnIPTV: "+status)
                #client.GUI.ActivateWindow({"window":"weather"})
                iptv.sendMessage(myip, ip, message)
                xbmc.executebuiltin('Notification(eBahnIPTV,Message sent to groups thank you for using eBahnIPTV,10000,/logo.gif)')
            #else:
                #self.close()
                #self.dashboard()
       # else:
            #self.close()
            #self.dashboard()

    def enterPin(self):
        dialog = xbmcgui.Dialog()
        text = dialog.numeric(0, 'Enter your Pin')
        if(text=='111'): self.messagetype2()
        elif(text=='222'): xbmc.executebuiltin('XBMC.ActivateWindow(settings)')
        elif(text=='333'): xbmc.executebuiltin('XBMC.StartAndroidActivity("com.android.settings")')
        elif(text=='444'): xbmc.executebuiltin('XBMC.StartAndroidActivity("com.android.chrome")')
        else: xbmc.executebuiltin('XBMC.ActivateWindow(home)')
        
        '''
        
        kb = xbmc.Keyboard('default', 'heading', True)
        kb.setDefault('') # optional
        kb.setHeading('Please Enter your Pincode?') # optional
        #kb.setHiddenInput(True) # optional
        kb.doModal()
        if (kb.isConfirmed()):
            text = str(kb.getText())
            if(text=='111'): self.messagetype2()
            elif(text=='222'): xbmc.executebuiltin('XBMC.ActivateWindow(settings)')
            elif(text=='333'): xbmc.executebuiltin('XBMC.StartAndroidActivity("com.android.settings")')
            elif(text=='444'): xbmc.executebuiltin('XBMC.StartAndroidActivity("com.android.chrome")')
            else: xbmc.executebuiltin('XBMC.ActivateWindow(home)')
        
        kb = xbmc.Keyboard('default', 'heading')
        kb.setDefault('123') # optional
        kb.setHeading('Enter Pin (Press OK to cancel)') # optional
        #kb.setHiddenInput(True) # optional
        kb.doModal()
        if (kb.getText()!=''):
            ip = '192.168.0.' + kb.getText()
            kb = xbmc.Keyboard('default', 'heading')
            kb.setDefault('I need Laundry now!') # optional
            kb.setHeading('Enter Message to '+str(ip)+' press OK to cancel or Send') # optional
        #kb.setHiddenInput(True) # optional
            kb.doModal()
            if (kb.isConfirmed()):
                message = kb.getText()
                                
                msg = message.replace(' ', '%20')
                url = "%7B%22jsonrpc%22%3A%222.0%22%2C%22method%22%3A%22GUI.ShowNotification%22%2C%22params%22%3A%7B%22title%22%3A%22Message%20from%20"+myip+"%22%2C%22message%22%3A%22"+msg+"%22%7D%2C%22id%22%3A1%7D%22)"
                status = urllib2.urlopen('http://'+ip+':8080/jsonrpc?request='+url).read()
                xbmc.log("eBahnIPTV: "+status)
                #client.GUI.ActivateWindow({"window":"weather"})
                iptv.sendMessage(myip, ip, message)
                xbmc.executebuiltin('Notification(eBahnIPTV,Message sent to '+ip+',10000,/logo.gif)')
         '''    
        
if __name__ == '__main__':
    addon = MyAddon()
    addon.doModal()
    del addon
