import xbmc, xbmcgui, ConfigParser
import os, sys, platform, time, socket
import json,urllib2, datetime
from string import whitespace
from offline import vod
from offline import menus
import sqlite3

# testing
'''
import sqlite3
cn = sqlite3.connect('ebahniptv.db')
print "Connected!"
'''
try: from sqlite3 import dbapi2 as sqlite
except: print ''
DB = os.path.join(xbmc.translatePath("special://database"), 'ebahniptv.db')
db = sqlite.connect(DB)

# create
try: db.execute('''CREATE TABLE users(id INT PRIMARY KEY NOT NULL, username TEXT, password TEXT, roomno INT);''')
except: print ''

#insert
try:
    db.execute("INSERT INTO users (id, username, password, roomno) VALUES (2, 'user2', '222', 2222)");
    db.commit()
except: print ''

#select
cursor = db.execute("SELECT * from users")
for row in cursor:
   print "id = ", row[0]
   print "username = ", row[1]
   print "password = ", row[2]
   print "roomno = ", row[3], "\n"

#update
db.execute("UPDATE users set username = 'user11' where id=1")
db.commit
print "Total number of rows updated :", db.total_changes

cursor = db.execute("SELECT * from users")
for row in cursor:
   print "id = ", row[0]
   print "username = ", row[1]
   print "password = ", row[2]
   print "roomno = ", row[3], "\n"

#delete
db.execute("DELETE from users where ID=2;")
db.commit
print "Total number of rows deleted :", db.total_changes

cursor = db.execute("SELECT * from users")
for row in cursor:
   print "id = ", row[0]
   print "username = ", row[1]
   print "password = ", row[2]
   print "roomno = ", row[3], "\n"
   
xbmcgui.Dialog().ok('IPTV', 'inserted')

db.close()


##rows = db.execute('SELECT * FROM movie WHERE c07 > 2010') 


#path = xbmc.getInfoLabel('ListItem.FileNameAndPath')
'''
win = xbmcgui.Window(xbmcgui.getCurrentWindowId())
curctl = win.getFocusId()
cursel = curctl.getSelectedItem()
print 'mm1:'+str(curctl)
'''
# dont delete
'''
import xbmc
DBID = xbmc.getInfoLabel('ListItem.DBID')
pseudoItem = loadItem(DBID) # << My problem is right here!
pseudoItem.setProperty(name, value) # << Make DB changes here 
'''
class DB:
    iptvCount = 0
    ip = ''
    serveraddress = 'http://192.168.0.4:1313/ebahniptv/'
    servertime = ''
    serveronline = False
    licensedays = ''
    prop = {'name': 'value'};
    roomno = ''
    guestname = ''
    if(os.name=='nt'): serveraddress = 'http://127.0.0.1:1313/ebahniptv/'
    if(os.name=='posix'): serveraddress = 'http://192.168.0.4:1313/ebahniptv/'
    imglocation = 'special://skin/images/menus/'
    vod = vod
    menus = menus
    
    
    if xbmc.getInfoLabel('Network.IPAddress') != None:
        ip = str(xbmc.getInfoLabel('Network.IPAddress'))
        devmode = False
    if "192." not in ip:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("gmail.com",80))
        ip = s.getsockname()[0]
        s.close()
        devmode = True
    
    def __init__(self):      
        socket.setdefaulttimeout(2)
        
        self.servertime = self.getTime()
        
        DB.iptvCount += 1
        self.roomno = '123'
        self.guestname = "Mr. James"
        
        # set default properties
        try:
            xbmcgui.Window(10000).setProperty('temp2','22')
            xbmcgui.Window(10000).setProperty('temp2','22')
            if self.serveronline: xbmcgui.Window(10000).setProperty('serverstatus','special://skin/images/icons/online.png')
            else: xbmcgui.Window(10000).setProperty('serverstatus','special://skin/images/icons/offline.png')
        except: print ''
     
    def test(self):
        print 'hello'
           
    def displayCount(self):
        print "Total IPTV %d" % DB.iptvCount
        
    def getTime(self):
        rt = ''
        try:
            req = urllib2.Request(self.serveraddress+'json?q=time' , None, headers={})
            resp = urllib2.urlopen(req).read()
            rt = resp.translate(None, '"').translate(None, whitespace)
            self.serveronline = True
        except: rt = ''
        return rt
        
    def getServerTime(self):
        req = urllib2.Request('http://192.168.0.4:8080/json.jsp?q=time' , None, headers={})
        resp = urllib2.urlopen(req).read()
        servertime = resp.translate(None, '"').translate(None, whitespace)
        return servertime
        
    def displayIPTV(self):
        print "IP : ", self.ip, ", Title: ", self.title
        
    def getProperties(self):
        global json
        req = urllib2.Request(self.serveraddress+'json?q=modules')
        print self.serveraddress+'json?q=modules'
        opener = urllib2.build_opener()
        f = opener.open(req)
        json = json.loads(f.read())
        for pr in json:
            name = str(pr)
            value = str(json[pr])
            self.prop[name] = value
            try:
                xbmcgui.Window(10000).setProperty(name, value)
                print name+'=='+value
            except: print ''
  
    def getLicenses(self, code):
        req = urllib2.Request(self.serveraddress+'json?q=license&code='+code , None, headers={})
        resp = urllib2.urlopen(req).read()
        licensedays = resp.translate(None, '"').translate(None, whitespace)
        return licensedays
    
    def getChannels(self):
        n = 50; ch = [[['' for k in xrange(n)] for j in xrange(n)] for i in xrange(n)]
        req = urllib2.Request(self.serveraddress+'json?q=channels')
        opener = urllib2.build_opener()
        f = opener.open(req)
        ch = json.loads(f.read())
        return ch
    
    def getVod(self):
        n = 50; vd = [[['' for k in xrange(n)] for j in xrange(n)] for i in xrange(n)]
        req = urllib2.Request(self.serveraddress+'json?q=vod')
        opener = urllib2.build_opener()
        f = opener.open(req)
        vd = json.loads(f.read())
        return vd
    
    def getWatched(self, ip):
        n = 50; wt = [[['' for k in xrange(n)] for j in xrange(n)] for i in xrange(n)]
        req = urllib2.Request(self.serveraddress+'json?q=watched')
        opener = urllib2.build_opener()
        f = opener.open(req)
        wt = json.loads(f.read())
        return wt
    
    def getMessages(self, ip):
        n = 50; msg = [[['' for k in xrange(n)] for j in xrange(n)] for i in xrange(n)]
        req = urllib2.Request(self.serveraddress+'json?q=messages')
        opener = urllib2.build_opener()
        f = opener.open(req)
        msg = json.loads(f.read())
        return msg
    
    def sendMessage(self, fromaddr, toaddr, message):
        req = urllib2.Request(self.serveraddress+'json?q=newmessage&from='+fromaddr+'&to='+toaddr+'&message='+message , None, headers={})
        resp = urllib2.urlopen(req).read()
        msg = resp.translate(None, '"').translate(None, whitespace)
        return msg
    
    def messageType(self):
        msgtype = ['Loundry','Room Services', 'No Electricity','No Water','Food']
        return msgtype
    
    def getWeather(self, code):    
        n = 10; rt = [[['' for k in xrange(n)] for j in xrange(n)] for i in xrange(n)]
        req = urllib2.Request(self.serveraddress+'json?q=weather&code='+code)
        opener = urllib2.build_opener()
        f = opener.open(req)
        rt = json.loads(f.read())
        return rt
    
    def setProperties(self):
        print 'mm1'+os.getcwd()
        cn = sqlite3.connect('ebahniptv.db')
        n = 100; oo = [[['' for k in xrange(n)] for j in xrange(n)] for i in xrange(n)]
        cursor = cn.execute("SELECT * FROM modules")
        cols = list(map(lambda x: x[0], cursor.description))
        #rows = list (1000)
        row = [None]*len(cols)
        rows = {}
        #row = {}
        for rw in cursor:
            for cl in range(0,7):
                name = rw[1]+cols[cl][0:2]
                value = rw[cl]
                try: xbmcgui.Window(10000).setProperty(name, value)
                except: print ''       
        cn.close()
        
    def execute(self, menuname):
        if(menuname=='main'): xbmc.executebuiltin('XBMC.ActivateWindow(134)')
        elif(menuname=='tv'): xbmc.executebuiltin('XBMC.ActivateWindow(2222)')
        elif(menuname=='hotel'): xbmc.executebuiltin('XBMC.ActivateWindow(4444)')
        else: xbmc.executebuiltin('XBMC.RunScript(special://home/addons/script.ebahniptv/' + menuname + '.py)')

    def getTemp(self):
        temp = ['28','25','22','26','27']
        xbmcgui.Window(10000).setProperty('temp1name','Monday')
        xbmcgui.Window(10000).setProperty('temp2name','Tuesday')
        xbmcgui.Window(10000).setProperty('temp3name','Wednesday')
        xbmcgui.Window(10000).setProperty('temp4name','Thursday')
        xbmcgui.Window(10000).setProperty('temp5name','Friday')
        xbmcgui.Window(10000).setProperty('temp1','21')
        xbmcgui.Window(10000).setProperty('temp2','22')
        xbmcgui.Window(10000).setProperty('temp3','23')
        xbmcgui.Window(10000).setProperty('temp4','24')
        xbmcgui.Window(10000).setProperty('temp5','25')
        xbmcgui.Window(10000).setProperty('temp1title','Day 1')
        xbmcgui.Window(10000).setProperty('temp2title','Day 2')
        xbmcgui.Window(10000).setProperty('temp3title','Day 3')
        xbmcgui.Window(10000).setProperty('temp4title','Day 4')
        xbmcgui.Window(10000).setProperty('temp5title','Day 5')
        return temp

'''
emp1 = IPTV("192.168.0.8", "Room 1")
emp2 = IPTV("192.168.0.10", "Room 2")
emp1.displayIPTV()
emp2.displayIPTV()
print "Total Employee %d" % IPTV.iptvCount
'''
            
################################## TESTING ###############################################
#xbmc.executebuiltin('XBMC.RunScript(script.service.alarmclock)')
#xbmc.executebuiltin('XBMC.RunScript(special://userdata/scripts/alarm.py)')
            

